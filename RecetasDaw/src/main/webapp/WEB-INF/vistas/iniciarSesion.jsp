<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<style>
body {
  margin: 0;
  padding: 0;
  background-color: #17a2b8;
}
#login .container #login-row #login-column #login-box {
  margin-top: 1px;
  max-width: 600px;
  height: 360px;
  border: 2px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 10px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->
</head>
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Login form</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="/login.ctr" method="post">
                            <h3 class="text-center text-primary">Iniciar Sesión</h3>
                            <div class="form-group">
                                <label for="email" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Email:</label><br>
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Contraseña:</label><br>
                                <input type="text" name="password" id="password" class="form-control">
                            </div>                           
                            <div class="form-group">                             
                                <input type="submit" name="submit" class="btn btn-primary" value="Iniciar Sesión">
                            </div>                          
                            <br>
                            <div id="register-link" class="text-right">
                                <a href="/registro.ctr" class="text-right">Registrate aquí</a>
                            </div>
                        </form>                       
                    </div>
                </div>
            </div>
        </div>
    </div>    

</body>
<br><br>
<!-- Start Footer -->
<%@ include file="utils/footer.html" %>
<!-- End Footer -->
	
<script>	
window.onload = iniciarSesion();

function iniciarSesion(){
	var login = "${login}";

	if(login =="noUser"){
		swal({
			title: 'UPSSS!!',
			text: 'Autenticación incorrecta',
			icon: 'error'
		});
	}else if(login =="user" || login =="admin"){
		swal({
			title: 'Bien!!',
			text: 'Autenticación correcta',
			icon: 'success'
		});
	}
}
</script>
</html>