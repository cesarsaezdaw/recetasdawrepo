<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<style>
body {
  margin: 0;
  padding: 0;
  background-color: #17a2b8;
}
#login .container #login-row #login-column #login-box {
  margin-top: 1px;
  max-width: 600px;
  height: 600px;
  border: 2px solid #9C9C9C;
  background-color: #EAEAEA;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 10px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: 0px;
}
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->
</head>
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Registro form</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="/registrarUsuario.ctr" method="post">
                            <h3 class="text-center text-primary">Registro de Usuario</h3>
                            <div class="form-group">
                                <label for="email" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Email:</label><br>
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Contraseña:</label><br>
                                <input type="text" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="nombre" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Nombre:</label><br>
                                <input type="text" name="nombre" id="nombre" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="apellidos" class="col-12 col-xl-8 h5 text-left text-primary pt-3">Apellidos:</label><br>
                                <input type="text" name="apellidos" id="apellidos" class="form-control">
                            </div>
                            <br>
                            <div class="form-group">                             
                                <input type="submit" name="submit" onclick="return validar()" class="btn btn-primary" value="Registrarse">
                            </div>                                                   
                        </form>                       
                    </div>
                </div>
            </div>
        </div>
    </div>    

</body>
<br><br>

<!-- Start Footer -->
<%@ include file="utils/footer.html" %>
<!-- End Footer -->
	
<script>	
window.onload = iniciarSesion();

function iniciarSesion(){
	var login = "${login}";
	if(login =="noUser"){
		swal({
			title: 'UPSSS!!',
			text: 'Autenticación incorrecta',
			icon: 'error'
		});
	}else if(login =="user" || login =="admin"){
		swal({
			title: 'Bien!!',
			text: 'Autenticación correcta',
			icon: 'success'
		});
	}
	
	var eliminadoUsuario = "${eliminadoUsuario}";
	if(eliminadoUsuario == 'true'){
		swal({
			title: 'Bien!!',
			text: 'Usuario eliminado correctamente',
			icon: 'success'
		});
	}else if(eliminadoUsuario == 'false'){
		swal({
			title: 'Upss!!',
			text: 'Error al eliminar el usuario',
			icon: 'error'
		});
	}
}

function validar() { 
	
	  var validacion = false;	 
	  var email = $("#email").val();
	  var password = $("#password").val();
	  var nombre = $("#nombre").val();
	  var apellidos = $("#apellidos").val();	 
	  var arrayDatos = [email,password,nombre,apellidos];
	  var arrayNombre =["Email","Contraseña","Nombre","Apellidos"];
	  var arrayError = [];
	  
	  for(var i=0;i<arrayDatos.length;i++){
		  if(arrayDatos[i]==null || arrayDatos[i]==''){
			  arrayError.push("\n - "+ arrayNombre[i]);			  
		  }		  		  
	  }

	  if(arrayError.length>0){
		  swal({
				title: '¡Uppsss!',
				text: 'No pueden estar vacios los campos: \n' + arrayError,
				icon: 'error'
			});
		  validacion = false;
	  }else{//validamos si son correctos los datos
	  
		  var listaEmails = "${listEmail}";
		  var listaEmailsString = listaEmails.substring(1,listaEmails.length-1);		 		
		  var arrayEmails = listaEmailsString.split(",");		 
		  var repetido = false;
		  for(var i=0;i<arrayEmails.length;i++){
			  if(arrayEmails[i].trim() == email.trim()){				  
				  repetido = true;
			  }		  
		  }
		  if(repetido){
			  swal({
					title: '¡Uppsss!',
					text: 'Ya existe el usario registrado \n con el Email: ' + email,
					icon: 'error'
				});			  
			  validacion = false;
		  }else{				  
			  validacion = true;
		  }		 
	  }
	return validacion;
}
</script>
</html>