<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<style>
.flex-container{
	display:flex;
	background-color: white;
	/*padding:10px;*/
	/*border: 2px solid black;*/
}
.flex-container div{
	background-color: white;	
	/*border: 2px solid black;*/ 
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
  border: 2px solid black;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 1px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}

</style>
<body>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->
 
 <!-- Start Contact -->
    <section class="container py-5">
		<div class="flex-container">
			<div style="flex-basis:60%">
				<h1 class="col-12 col-xl-12 h2 text-left text-primary pt-3">${receta.nombre}</h1>
				<div style="margin-left:10px">
					<form id="addFavoritosForm" class="contact-form row" method="post" action="/addFavoritas.ctr" role="form">
            			<button type="submit" onclick="return validar()" class="btn rounded-pill px-4 btn-outline-primary mb-3 center" style="width:30%">
            				Añadir a favoritos
            			</button>
            			<input  type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
        			</form>
				</div>
        		       
        		<p class="col-12 col-xl-12 text-left text-muted pb-5 light-300">${receta.descripcion}</p>
			</div>
			<div style="flex-basis:40%; margin-left:5%">
				<div style="height:50px">
					
					<p style="font-size:35px;text-align:center" class="text-primary">Visitas: ${receta.visualizaciones}</p>					
                    
				</div >
				<div>
					<table>
  						<tr>   
  						    <th style="width:20%;text-align: center;">TOP 5</th>					
    						<th style="width:75%;text-align: center;">Receta</th>
    						<th style="width:15%;text-align: center;">Visitas</th>
  						</tr>
  						<c:forEach var="receta" items="${topRecetas}">
  							<tr>
  								<td style="text-align: center">
  									<form id="formulario" class="contact-form row" method="post" action="/datosReceta.ctr" role="form">
  										<button style="width:50%;margin-left:30px;" type="submit" class="btn rounded-pill btn-outline-primary center">Ver</button>
  										<input type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
  									</form>	
  								</td>
    							<td style="text-align: left">${receta.nombre}</td>
    							<td style="text-align: center">${receta.visualizaciones}</td>
  							</tr>
  						</c:forEach>  						
					</table>
				</div>
				
			</div>
					
		</div>
        

        <div class="row pb-4 flex-container">
            <div class="col-lg-4">
                <div class="row mb-4">

                <img class="service card-img" src="./resources/img/recetas/${receta.tipoAlimento}/${receta.foto}" alt="Card image" height="300px" width="400px"/>  
                   
                </div>
            </div>

            <div class="col-lg-8 ">              
            	<div class="col-12">                   
                  <h2><label><b>Preparación</b></label></h2>
                  <c:forEach var="paso" items="${pasos}">
                  	<label>${paso.numPaso}. ${paso.descripcion}</label><br/>
                  </c:forEach>
                  <br>
                  <c:if test="${receta.notas != ''}">
                  	<h4><label><b>Notas</b></label></h4>
                  	<label>${receta.notas}</label><br/>
                  </c:if>                 
                </div>                                
            </div>

        </div>
    </section>


<!-- Start Footer -->
<%@ include file="utils/footer.html" %>
<!-- End Footer -->	
	
<script>
window.onload = iniciaDatosReceta();

function iniciaDatosReceta(){	
	//compueba receta favorita
	var recetas = "${favoritasListStr}";	
	recetas = recetas.substring(1, recetas.length-1);
	
	var recetasArray = [];
	recetasArray = recetas.split(",");
	
	var recetaSeleccionada = "${receta.nombre}";
	var esFavorita=false;
	for(var i=0; i<recetasArray.length;i++){
		if(recetaSeleccionada.trim() == recetasArray[i].trim()){
			esFavorita=true;
		}
	}
		
	if("${aniadida}"){
		esFavorita = true;
		swal({
			title: 'Bien!!',
			text: 'Receta añadida correctamente',
			icon: 'success'
		});
	}
	
	if(esFavorita){
		$('#addFavoritosForm').hide();//oculto formulario y botón para añadir favoritos
	}
	
	if("${recetaCreada}"){
		swal({
			title: '¡Hecho!',
			text: 'Receta creada!!',
			icon: 'success'
		});
	}
	
	var modificada = "${modificada}";
	if(modificada == 'true'){
		swal({
			title: '¡Hecho!',
			text: 'Receta modificada!!',
			icon: 'success'
		});
	}else if(modificada == 'false'){
		swal({
			title: '¡Upss!',
			text: 'Algo ha ido mal al modificar la receta!!',
			icon: 'error'
		});
	}
}

function validar() {
	var validacion = false;
	var login ="${login}";
	if(login == 'user' || login == 'admin'){
		validacion = true;
	}else{
		swal({
			title: 'Upss!!',
			text: 'Registrate para añadir favoritos',
			icon: 'error'
		});
	}	
	return validacion;
}

</script>
</body>
</html>
 