<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->

	<!-- Filtro Tipos de Alimento-->
    <section class="service-wrapper py-3">
        <div class="service-tag py-5 bg-secondary">
            <div class="col-md-12">
                <ul class="nav d-flex justify-content-center">
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary active shadow rounded-pill text-light px-4 light-300" href="#" data-filter=".project">Todos</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Carne">Carne</a>
                    </li>
                    <li class="filter-btn nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Pescado">Pescado</a>
                    </li>                
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Arroz">Arroz</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Pasta">Pasta</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Legumbre">Legumbre</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Postre">Postre</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

 <!-- Start Pricing Horizontal Section -->
    <section class="bg-light pt-sm-0 py-5">
        <div class="container py-5">
            <h1 class="h2 semi-bold-600 text-center mt-2">Mis Recetas Creadas</h1>
            <p class="text-center pb-5 light-300">Gestiona tus recetas creadas</p>
			<div class="row projects ">
            	<c:forEach var="receta" items="${recetas}">            
         			<div class="project ${receta.tipoAlimento}"> 
            			<div class="pricing-horizontal row col-10 m-auto d-flex shadow-sm rounded overflow-hidden bg-white">
                			<div class="pricing-horizontal-icon col-md-3 text-center bg-secondary text-light py-4">
                				<img class="service card-img" src="./resources/img/recetas/${receta.tipoAlimento}/${receta.foto}" alt="Card image">              
                			</div>
                			<div class="pricing-horizontal-body offset-lg-1 col-md-5 col-lg-4 d-flex align-items-center pl-5 pt-lg-0 pt-4">
                    			<ul class="text-left px-2 list-unstyled mb-0 light-300">
                     				<h4><b>${receta.nombre}</b></h4>
                        			<li><i class="bx bxs-circle me-2"></i>Duración: ${receta.duracion} </li>
                        			<li><i class="bx bxs-circle me-2"></i>Comensales: ${receta.numeroPersonas} </li>
                        			<li><i class="bx bxs-circle me-2"></i>País: ${receta.pais}</li>
                    			</ul>                                  
                			</div>
                			<div class="pricing-horizontal-tag col-md-4 text-center pt-3 d-flex align-items-center">
                    			<div class="w-100 light-300">
                    				<form id="verRecetaForm" class="contact-form row" method="post" action="/datosReceta.ctr" role="form">
            							<button type="submit" class="btn rounded-pill px-4 btn-outline-primary mb-3" style="width:60%">
            								Ver Receta
            							</button>
            							<input type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
                    					<input type="hidden" id="descripcionReceta" name="descripcionReceta" value="${receta.descripcion}">                   		
                    					<input type="hidden" id="notasReceta" name="notasReceta" value="${receta.notas}">
                    					<input type="hidden" id="paisReceta" name="paisReceta" value="${receta.pais}">
                    					<input type="hidden" id="tipoAlimentoReceta" name="tipoAlimentoReceta" value="${receta.tipoAlimento}">
                    					<input type="hidden" id="duracionReceta" name="duracionReceta" value="${receta.duracion}">
                    					<input type="hidden" id="numeroPersonasReceta" name="numeroPersonasReceta" value="${receta.numeroPersonas}">
                    					<!--  <input type="hidden" id="fotoReceta" name="fotoReceta" value="${receta.foto}">-->
            						</form>
            						<br>
            						<form id="editarRecetaCreadaForm" class="contact-form row" method="post" action="/editarReceta.ctr" role="form">
            							<button type="submit" class="btn rounded-pill px-4 btn-outline-primary mb-3" style="width:60%">
                        					Editar Receta
                        				</button>
                        				<input type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
                        			</form>
                        			<br/>
            						<form id="eliminarRecetaCreadaForm" class="contact-form row" method="post" action="/eliminarMiRecetaCreada.ctr" role="form">
            							<button type="submit" class="btn rounded-pill px-4 btn-outline-primary mb-3" style="width:60%">
                        					Eliminar Receta
                        				</button>
                        				<input type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
                        			</form>
                    			</div>                                    
                			</div>
            			</div>
            			<br>
            		</div>
            	</c:forEach>
  			</div>          
    	</div>
    </section>

<!-- Start Footer -->
<%@ include file="utils/footer.html" %>
<!-- End Footer -->

<script>	
window.onload = iniciaMisRecetasCreadas();

function iniciaMisRecetasCreadas(){
	
	if("${eliminada}"){
		swal({
			title: 'Bien!!',
			text: 'Receta eliminada correctamente',
			icon: 'success'
		});
	}
}
</script>
</body>
</html>