<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- End Header -->

<!-- Filtro Tipos de Alimento-->
    <section class="service-wrapper py-3">
        <div class="service-tag py-5 bg-secondary">
            <div class="col-md-12">
                <ul class="nav d-flex justify-content-center">
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary active shadow rounded-pill text-light px-4 light-300" href="#" data-filter=".project">Todos</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Carne">Carne</a>
                    </li>
                    <li class="filter-btn nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Pescado">Pescado</a>
                    </li>                
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Arroz">Arroz</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Pasta">Pasta</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Legumbre">Legumbre</a>
                    </li>
                    <li class="nav-item mx-lg-4">
                        <a class="filter-btn nav-link btn-outline-primary rounded-pill text-light px-4 light-300" href="#" data-filter=".Postre">Postre</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Start Service -->
    <section class="container overflow-hidden py-5">
        <div class="row gx-5 gx-sm-3 gx-lg-5 gy-lg-5 gy-3 pb-3 projects">
			<c:forEach var="receta" items="${recetas}">			
				<form id="formulario" class="contact-form row" method="post" action="/datosReceta.ctr" role="form">
            	<div class="col-xl-3 col-md-4 col-sm-6 project ${receta.tipoAlimento}">           	
            		<button type="submit" style="width:325px;heigth:220px;background-color:transparent;border:none;margin: 5% auto">
                		<a href="#" class="service-work card border-0 text-white shadow-sm overflow-hidden mx-5 m-sm-0">
                    		<img class="service card-img" src="./resources/img/recetas/${receta.tipoAlimento}/${receta.foto}" alt="Card image" width="325px" height="220px">
                    		<div class="service-work-vertical card-img-overlay d-flex align-items-end">
                        		<div class="service-work-content text-left text-light">
                            		<span class="btn btn-outline-light rounded-pill mb-lg-3 px-lg-4 light-300">${receta.tipoAlimento}</span>
                            		<p class="card-text">${receta.nombre}</p>
                        		</div>
                    		</div>                    	
                		</a>                 		
                	</button>
            	</div>
            		<input type="hidden" id="nombreReceta" name="nombreReceta" value="${receta.nombre}">
                    <input type="hidden" id="descripcionReceta" name="descripcionReceta" value="${receta.descripcion}">                    
                    <input type="hidden" id="notasReceta" name="notasReceta" value="${receta.notas}">
                    <input type="hidden" id="paisReceta" name="paisReceta" value="${receta.pais}">
                    <input type="hidden" id="tipoAlimentoReceta" name="tipoAlimentoReceta" value="${receta.tipoAlimento}">
                    <input type="hidden" id="duracionReceta" name="duracionReceta" value="${receta.duracion}">
                    <input type="hidden" id="numeroPersonasReceta" name="numeroPersonasReceta" value="${receta.numeroPersonas}">
                    <!-- <input type="hidden" id="fotoReceta" name="fotoReceta" value="${receta.foto}"> -->
            	</form>  
	        </c:forEach>	      	        
        </div>
    </section>

<!-- Start Footer -->
<%@ include file="utils/footer.html" %>
<!-- End Footer -->

</body>
<script>	
window.onload = iniciaIndex();

function iniciaIndex(){
	var login = "${login}";
	var sesionCerrada = "${sesionCerrada}";
	if(sesionCerrada){				
		swal({
			title: 'Hasta Pronto!!',
			text: 'Sesión Cerrada,
			icon: 'success'
		});		
	}

}
</script>
</html>