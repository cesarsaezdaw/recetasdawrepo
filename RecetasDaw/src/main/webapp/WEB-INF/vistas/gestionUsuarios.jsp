<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->

 <!-- Start Pricing Horizontal Section -->
    <section class="bg-light pt-sm-0 py-5">
        <div class="container py-5">
            <h1 class="h2 semi-bold-600 text-center mt-2">Gestión de Usuarios</h1>        
			<br>
			<div class="row projects ">
            	<c:forEach var="usuario" items="${usuarios}">           
         			<div class="project ${usuario.nombre}"> 
            			<div class="pricing-horizontal row col-10 m-auto d-flex shadow-sm rounded overflow-hidden bg-white">
                			<div class="pricing-horizontal-icon col-md-3 text-center bg-secondary text-light py-4"  style="width:150px">
                				<img class="service card-img" src="./resources/img/usuario/usuario.png" alt="Card image" >
                			</div>
                
                			<div class="pricing-horizontal-body offset-lg-1 col-md-5 col-lg-4 d-flex align-items-center pl-5 pt-lg-0 pt-4">
                    			<ul class="text-left px-2 list-unstyled mb-0 light-300">   
                    				<li><i class="bx bxs-circle me-2"></i>Correo: <b>${usuario.email}</b></li>                 	
                        			<li><i class="bx bxs-circle me-2"></i>Nombre: <b>${usuario.nombre}</b></li>
                        			<li><i class="bx bxs-circle me-2"></i>Apellidos: <b>${usuario.apellidos}</b></li>
                        			<li><i class="bx bxs-circle me-2"></i>Administrador: <b>${usuario.administrador}</b></li>                       
                    			</ul>      
                			</div>
                			<div class="pricing-horizontal-tag col-md-4 text-center pt-3 d-flex align-items-center">
                    			<div class="w-100 light-300">   
                    			<c:if test="${usuario.email ne 'admin@admin.es'}">                	
            						<form id="cambioAdministracionUsuarioForm" class="contact-form row" method="post" action="/cambioAdministracionUsuario.ctr" role="form">
            							<button type="submit" class="btn rounded-pill px-4 btn-outline-primary mb-3" style="width:60%">
                        					Cambio Administración
                        				</button>
                        				<input type="hidden" id="email" name="email" value="${usuario.email}">
                        			</form>
                       
            						<form id="eliminarUsuarioForm" class="contact-form row" method="post" action="/eliminarUsuario.ctr" role="form">
            							<button type="submit" class="btn rounded-pill px-4 btn-outline-primary mb-3" style="width:60%">
                        					Eliminar Usuario
                        				</button>
                        				<input type="hidden" id="email" name="email" value="${usuario.email}">
                        			</form>
                        		</c:if>	
                    			</div>                                     
                			</div>
            			</div>
            			<br>
            		</div>
            	</c:forEach>
  			</div>          
    	</div>
    </section>

    <!-- Start Footer -->
<%@ include file="utils/footer.html" %>
	<!-- End Footer -->
	
<script>	
window.onload = iniciaGestionUsuarios();

function iniciaGestionUsuarios(){
	var cambioAdministracionUsuario = "${cambioAdministracionUsuario}";
	var eliminadoUsuario = "${eliminadoUsuario}";
	
	if(cambioAdministracionUsuario == 'true'){
		swal({
			title: 'Bien!!',
			text: 'Se ha modificado el perfil del Usuario',
			icon: 'success'
		});
	}else if(cambioAdministracionUsuario == 'false'){
		swal({
			title: 'Upss!!',
			text: 'Error al modificar el perfil del usuario',
			icon: 'error'
		});
	}
	
	if(eliminadoUsuario == 'true'){
		swal({
			title: 'Bien!!',
			text: 'Usuario eliminado correctamente',
			icon: 'success'
		});
	}else if(eliminadoUsuario == 'false'){
		swal({
			title: 'Upss!!',
			text: 'Error al eliminar el usuario',
			icon: 'error'
		});
	}
}
</script>
</body>
</html>