<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<body>

<!-- Header -->
<%@ include file="utils/header.html" %>
<!-- Close Header -->

 <!-- Start Pricing Horizontal Section -->
    <section class="bg-light pt-sm-0 py-5">
        <div class="container py-5">

            <h1 class="h2 semi-bold-600 text-center mt-2">Nueva Receta</h1>
            <p class="text-center pb-5 light-300">Cree una nueva receta</p>

			<%-- enctype="multipart/form-data"/crearReceta.ctr--%>
			<form id="crearRecetaForm" class="contact-form row" method="post" action="/crearReceta.ctr" enctype="multipart/form-data" role="form">
          
            		<div class="shadow-sm py-sm-0 py-5" style="background-color:#889AFF">
                    	<div class="row p-3">
                    	
                        	<div class="col-lg-6 mb-4">
                        		<div class="form-floating">
                            		<input type="text" class="form-control form-control-lg light-300" id="nombreReceta" name="nombreReceta">
                            		<label for="nombreReceta light-300">Nombre receta</label>
                          		
                        		</div>
                    		</div><!-- End Input Name -->
                    		
                    		<div class="col-lg-6 mb-4">                     		
                    			<span class="btn btn-primary" style="width:100%;height:60px">
                        			<input class="btn btn-primary" type="file" name="fotoReceta" id="fotoReceta">
                    			</span>
                    		</div><!-- End Input Email -->
            				
                    		<div class="col-12">
                        		<div class="form-floating mb-3">
                            		<textarea class="form-control light-300" id="descripcionReceta"  name="descripcionReceta"></textarea>
                            		<label for="descripcionReceta light-300">Descripción</label>
                        		</div>
                    		</div><!-- End Textarea Message -->
                   			<div class="col-12">
                        		<div class="form-floating mb-4">
                            		<input type="text" class="form-control form-control-lg light-300" id="notasReceta" name="notasReceta">
                            		<label for="notasReceta light-300">Notas</label>
                        		</div>
                    		</div><!-- End Input Subject -->
                    		
                    		
                    		<div class="col-lg-6 mb-4">
                        		<div class="form-floating">                       	
                            		<select name="paisReceta" id="paisReceta" class="form-control form-control light-300">
                            			<option value=""></option>
                            			<c:forEach var="pais" items="${paises}">
                            				<option value="${pais.nombre}" class="form-control form-control-lg light-300">${pais.nombre}</option>
                            			</c:forEach>
                            		</select>
                            		<label for="PaisReceta light-300">País</label>                           		
                        		</div>
                    		</div><!-- End Input Name -->

                    		<div class="col-lg-6 mb-4">
                        		<div class="form-floating">                           		                           		
                        			<select name="tipoAlimentoReceta" id="tipoAlimentoReceta" class="form-control form-control light-300">
                            			<option value=""></option>
                            			<c:forEach var="tipoAlimento" items="${tiposAlimentos}">
                            				<option value="${tipoAlimento.nombre}" class="form-control form-control-lg light-300">${tipoAlimento.nombre}</option>
                            			</c:forEach>
                            		</select>
                            		<label for="tipoAlimentoReceta light-300">Tipo de alimento</label>		
                        		</div>
                    		</div><!-- End Input Email -->
                    		
                    		<div class="col-lg-6 mb-4">
                        		<div class="form-floating">                            	                           		
                            		<select name="duracionReceta" id="duracionReceta" class="form-control form-control light-300">
                            			<option value=""></option>
                            			<option value="15m" class="form-control form-control-lg light-300">15m</option>
                            			<option value="30m" class="form-control form-control-lg light-300">30m</option>
                            			<option value="45m" class="form-control form-control-lg light-300">45m</option>
                            			<option value="1h" class="form-control form-control-lg light-300">1h</option>
                            			<option value="1h 15m" class="form-control form-control-lg light-300">1h 15m</option>
                            			<option value="1h 30m" class="form-control form-control-lg light-300">1h 30m</option>
                            			<option value="1h 45m" class="form-control form-control-lg light-300">1h 45m</option>
                            			<option value="2h" class="form-control form-control-lg light-300">2h</option>
                            			<option value="2h" class="form-control form-control-lg light-300">2h 30m</option>
                            			<option value="3h" class="form-control form-control-lg light-300">3h</option>
                            		</select>
                            		<label for="duracionReceta light-300">Duración</label>                     		
                        		</div>
                    		</div><!-- End Input Name -->

                    		<div class="col-lg-6 mb-4">
                        		<div class="form-floating">
                            		<select name="numeroPersonasReceta" id="numeroPersonasReceta" class="form-control form-control light-300">
                            			<option value=""></option>
                            			<option value="1" class="form-control form-control-lg light-300">1</option>
                            			<option value="2" class="form-control form-control-lg light-300">2</option>
                            			<option value="3" class="form-control form-control-lg light-300">3</option>
                            			<option value="4" class="form-control form-control-lg light-300">4</option>
                            			<option value="5" class="form-control form-control-lg light-300">5</option>
                            			<option value="6" class="form-control form-control-lg light-300">6</option>
                            			<option value="7" class="form-control form-control-lg light-300">7</option>
                            			<option value="8" class="form-control form-control-lg light-300">8</option>
                            		</select>
                            		<label for="numeroPersonasReceta light-300">Número de personas</label>	
                        		</div>
                    		</div><!-- End Input Email -->
                   	                     
                   		</div>                       
                	</div>
                	
                	<p></p>
                	
                	<h2><label for="preparacion">Preparación - Paso a paso:</label></h2>
      
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso1" name="preparacionRecetaPaso1">
                            <label for="preparacionRecetaPaso1 light-300">Paso 1</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso2" name="preparacionRecetaPaso2">
                            <label for="preparacionRecetaPaso2 light-300">Paso 2</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso3" name="preparacionRecetaPaso3">
                            <label for="preparacionRecetaPaso3 light-300">Paso 3</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso4" name="preparacionRecetaPaso4">
                            <label for="preparacionRecetaPaso4 light-300">Paso 4</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso5" name="preparacionRecetaPaso5">
                            <label for="preparacionRecetaPaso5 light-300">Paso 5</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso6" name="preparacionRecetaPaso6">
                            <label for="preparacionRecetaPaso6 light-300">Paso 6</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso7" name="preparacionRecetaPaso7">
                            <label for="preparacionRecetaPaso7 light-300">Paso 7</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso8" name="preparacionRecetaPaso8">
                            <label for="preparacionRecetaPaso8 light-300">Paso 8</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso9" name="preparacionRecetaPaso9">
                            <label for="preparacionRecetaPaso9 light-300">Paso 9</label>
                        </div>
                    </div>
                    
                    <div class="col-12">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control form-control-lg light-300" id="preparacionRecetaPaso10" name="preparacionRecetaPaso10">
                            <label for="preparacionRecetaPaso10 light-300">Paso 10</label>
                        </div>
                    </div>
                    
                    <input type="hidden"  id="preparacionReceta" name="preparacionReceta" value="">
                                        
                    <div class="col-md-12 col-12 m-auto text-end">
                        <button type="submit" onclick="return validar()" class="btn btn-secondary rounded-pill px-md-5 px-4 py-2 radius-0 text-light light-300">Crear Receta</button>
                    </div>    	
            </form>			
    </div>
    </section>
    <!--End Pricing Horizontal Section-->


    <!-- Start Footer -->
<%@ include file="utils/footer.html" %>
	<!-- End Footer -->
	
<script>
function validar() { 
	
	  var validacion = false;	  	  
	  var nombreReceta = $("#nombreReceta").val();
	  var fotoReceta =  $("#fotoReceta").val();
	  var descripcionReceta = $("#descripcionReceta").val();
	  var paisReceta = $("#paisReceta").val();
	  var tipoAlimentoReceta = $("#tipoAlimentoReceta").val();
	  var duracionReceta = $("#duracionReceta").val();
	  var numeroPersonasReceta = $("#numeroPersonasReceta").val();
	  var preparacionRecetaPaso1 = $("#preparacionRecetaPaso1").val();
	  
	  var arrayDatos = [nombreReceta,fotoReceta,descripcionReceta,paisReceta,tipoAlimentoReceta,duracionReceta,numeroPersonasReceta,preparacionRecetaPaso1];
	  var arrayNombre =["Nombre receta","Foto","Descripción","País","Tipo de alimento","Duración","Número de personas", "Preparación(al menos el Paso 1)"];
	  var arrayError = [];
	  for(var i=0;i<arrayDatos.length;i++){
		  if(arrayDatos[i]==null || arrayDatos[i]==''){
			  arrayError.push("\n - "+ arrayNombre[i]);			  
		  }		  		  
	  }

	  if(arrayError.length>0){
		  swal({
				title: '¡Uppsss!',
				text: 'No pueden estar vacios los campos: \n' + arrayError,
				icon: 'error'
			});
		  validacion = false;
	  }else{//validamos si son correctos los datos
		  //Nombre de receta repetido
		  var listaRecetas = "${listaRecetas}";
		  var listaRecetasString = listaRecetas.substring(1,listaRecetas.length-1);		 
		
		  var arrayRecetas = listaRecetasString.split(",");
		  var repetido = false;
		  for(var i=0;i<arrayRecetas.length;i++){
			  if(arrayRecetas[i].trim() == nombreReceta.trim()){				  
				  repetido = true;
			  }		  
		  }
		  if(repetido){
			  swal({
					title: '¡Uppsss!',
					text: 'Ya existe el nombre de receta: ' + nombreReceta,
					icon: 'error'
				});
			  
			  validacion = false;
		  }else{				 
			  var preparacion = [];
			  
			  preparacion[0] = $("#preparacionRecetaPaso1").val();			
			  preparacion[1] = $("#preparacionRecetaPaso2").val();
			  preparacion[2] = $("#preparacionRecetaPaso3").val();
			  preparacion[3] = $("#preparacionRecetaPaso4").val();
			  preparacion[4] = $("#preparacionRecetaPaso5").val();
			  preparacion[5] = $("#preparacionRecetaPaso6").val();
			  preparacion[6] = $("#preparacionRecetaPaso7").val();
			  preparacion[7] = $("#preparacionRecetaPaso8").val();
			  preparacion[8] = $("#preparacionRecetaPaso9").val();
			  preparacion[9] = $("#preparacionRecetaPaso10").val();
			  
			  $("#preparacionReceta").val(preparacion);  								
  
			  validacion = true;
		  }
		 
	  }
 return validacion;
}

</script>

</body>
</html>