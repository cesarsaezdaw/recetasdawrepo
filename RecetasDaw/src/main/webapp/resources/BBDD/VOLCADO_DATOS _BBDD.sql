-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "usuarios"
--

INSERT INTO "usuarios" ("email", "password", "nombre", "apellidos", "administrador") VALUES
('admin@admin.es', 'admin', 'Administrador', 'Administrador',true),
('maria@recetas.es', 'maria', 'Maria', 'Sanchez',false),
('pepe@recetas.es', 'pepe', 'Pepe', 'Herrero',false),
('jose@recetas.es', 'jose', 'José', 'Perez',false);

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "paises"
--

INSERT INTO "paises" ("nombre") VALUES
('Alemania'),
('España'),
('Francia'),
('Grecia'),
('Inglaterra'),
('Italia'),
('Portugal');

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "tipos_alimento"
--

INSERT INTO "tipos_alimento" ("nombre") VALUES
('Arroz'),
('Carne'),
('Legumbre'),
('Pasta'),
('Pescado'),
('Postre');

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "recetas"
--

INSERT INTO "recetas" ("nombre", "descripcion", "notas", "pais", "tipo_alimento","duracion", "numero_personas", "foto", "visualizaciones") VALUES
('Atún a la plancha con ajo y perejil', 'El atún es una de las mejores fuentes de ácidos grasos omega 3, fundamentales para la prevención de enfermedades cardiovasculares y otras patologías relacionadas con el sistema circulatorio, contiene un elevado porcentaje de proteínas, es rico en minerales esenciales como el magnesio, vitaminas del grupo B y E. Por todo ello, incluirlo en nuestra dieta está más que recomendado.', '', 'España', 'Pescado','15m', 2, 'atun_plancha.jpg', 0),
('Bacalao al pil-pil', 'El bacalao al pil-pil es una receta típica del País Vasco. Se suele realizar en una cazuela de barro, pero también podéis usar una olla tradicional para su elaboración. Para realizar esta receta, se necesitan muy pocos ingredientes, siendo el aceite de oliva virgen uno de sus ingredientes imprescindibles, pues es la base de la salsa que acompañaremos el bacalao.', '', 'España', 'Pescado','30m', 4, 'bacalao_al_pil_pil.jpg', 0),
('Confit de pato con mermelada de naranja', 'Confit de pato con mermelada de naranja', 'notas', 'España', 'Carne','1h 30m', 4, 'pato_naranja.jpg', 0),
('Costilla de ternera asada', 'Costilla de ternera asada', 'Consejo: a la hora de salar la carne, si ésta se fríe, asa o se hace a la plancha, es recomendable añadir la sal al final del cocinado para que la carne no pierda sus jugos.', 'España', 'Carne','1h 30m', 4, 'costilla_ternera.jpg', 0),
('Estofado de judías con tomate y cebolla', 'Este estofado de judías de la granja es un plato muy sencillo, pero muy sabroso. Las judías, como todas las legumbres, poseen muchos minerales y nutrientes y son una excelente fuente de proteína vegetal, fibra y carbohidratos de absorción lenta. ¡Te va a encantar este estofado de judías con tomate y cebolla saludable! Lo puedes comer solo o mejor acompañarlo de una ensalada o arroz con verduras.', 'notas', 'España', 'Legumbre','45m', 4, 'estofado_judias.jpg', 0),
('Lentejas viudas', 'Lentejas viudas, un guiso de cuchara sencillo de hacer y delicioso. Es conocido con este nombre debido a que no lleva nada de carne, solo verduras y, en algunos casos, patatas. Por ello, además, podemos decir que se trata de un plato de legumbres muy saludable, puesto que no lleva nada de grasa. Al momento de cocinar este plato de lentejas podemos escoger las verduras que más nos gusten. Eso sí, para potenciar el sabor recomendamos preparar un sofrito y añadirlo al guiso, así es, de hecho, como lo hacían nuestras abuelas.', 'Las calorías de las lentejas viudas varían en función de la cantidad de aceite utilizada en el sofrito, así como del total de verduras empleadas. No obstante, de forma aproximada podemos decir que 100 gramos contienen alrededor de 190-200 calorías, teniendo en cuenta la elaboración del sofrito.', 'España', 'Legumbre','1h', 4, 'lentejas_viudas.jpg', 0),
('Paella Valenciana', 'La paella valenciana es la más conocida de todas, forma parte de nuestra gastronomía y es muy popular fuera de nuestro país. Podemos preparar infinidad de paellas, ya que en Valencia son muchas las variedades que existen. Así pues, encontramos paella valenciana de marisco, paella valenciana mixta e incluso paella valenciana con alcachofas. No obstante, en esta ocasión vamos a mostrar la receta de paella valenciana auténtica, la original con garrofón, pollo y conejo.', 'notas', 'España', 'Arroz','1h 30m', 4, 'paella_valenciana.jpg', 0),
('Pizza de patata', 'Hoy en día, hay muchas alternativas de pizzas sin gluten para todos aquellos que sean intolerantes o alérgicos. Esta receta de pizza de patata es una de ellas, se trata de una preparación fácil y muy versátil con la que podrás preparar una pizza a tu gusto con los ingredientes que tengas por casa.', 'notas', 'España', 'Pasta','30m', 4, 'pizza_patata.jpg', 0),
('Salmón marinado al horno', 'Marinar es condimentar un alimento con 1 o varios elementos y dejar que por ósmosis se complementen y modifiquen cualidades, bien sea el sabor, el olor, la textura, el color, etc.', 'notas', 'España', 'Pescado','2h 30m', 4, 'salmon_horno.jpg', 0),
('Tarta de queso de La Viña', 'La famosa tarta de queso vasca de La Viña, también conocida como cheesecake de San Sebastián, es un famoso postre reconocido como una de las mejores tartas. Su elaboración es muy sencilla, así como lo es su increíble el resultado, pues queda jugosa, untuosa y deliciosa.', 'notas', 'España', 'Postre','45m', 6, 'tarta_queso.jpg', 0),
('Tarta de Santiago con chocolate', 'La tarta de Santiago o torta compostelana es un postre típico gallego elaborado a partir de pocos y sencillos ingredientes, que en conjunto logran una explosión de sabor con una textura que resultan únicos al paladar. De hecho, en la receta original solo se emplean azúcar, huevos y almendras molidas en sustitución de la harina de trigo.', 'notas', 'España', 'Postre','45m', 6, 'tarta_santiago.jpg', 0);

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "pasos_recetas"
--

INSERT INTO "pasos_recetas" ("nombre_receta", "numero_paso", "descripcion") VALUES
('Atún a la plancha con ajo y perejil',	1, 'Empezamos la receta de atún a la plancha pelando los dientes de ajo y picándolos finamente. Si lo prefieres, puedes utilizar ajo en polvo o, incluso, pasta de ajo. Por otro lado, la cantidad de ajo es orientativa, ya que en función de tus gustos puedes añadir más o menos.'),
('Atún a la plancha con ajo y perejil',	2, 'Cuando tengamos los dientes de ajo picados, los colocamos en un recipiente, añadimos el perejil, el aceite de oliva y mezclamos bien. Si quieres, también puedes añadir unas gotas de zumo de limón al aliño para potenciar todavía más el sabor del atún a la plancha.'),
('Atún a la plancha con ajo y perejil',	3, 'Ponemos la plancha o una sartén antiadherente a fuego alto con un chorrito de aceite. Cuando esté caliente, bajamos un poco la temperatura, cocinamos los filetes de atún, los volteamos y los cubrimos con la preparación anterior de ajo y perejil. El atún es pescado que se cocina muy rápido, por lo que deberás estar atento para evitar que se queme. En apenas cinco minutos lo tendrás listo.'),
('Atún a la plancha con ajo y perejil',	4, 'Podemos servir el atún a la plancha con ajo y perejil a modo de cena, sin nada más, o acompañado de patatas al horno, chips de calabacín o verduras salteadas. Y si todavía tienes más filetes de atún en la nevera y quieres probar nuevas recetas de pescado, te proponemos un plato un tanto más elaborado y calórico pero, sobre todo, delicioso: atún en salsa de setas.'),
('Bacalao al pil-pil', 1, 'Para comenzar con la receta de bacalao al pil-pil con colador, pela los dientes de ajo y corta en rodajas finas.'),
('Bacalao al pil-pil', 2, 'Pon el aceite de oliva en una olla amplia a fuego suave y calienta. Añade las láminas de ajo y cuece a fuego suave para que las láminas de ajo no se frían enseguida, sino que lo hagan lentamente y se vayan confitando.'),
('Bacalao al pil-pil', 3, 'Cuando los ajos estén casi listos, añade las guindillas y cocina 2 minutos más.'),
('Bacalao al pil-pil', 4, 'Retira con una espumadera los ajos y las guindillas y reserva. Pon en el mismo aceite los lomos de bacalao con la piel hacia arriba y cocina unos cuatro minutos por cada lado, procurando que no se frían. Tienen que salir pequeñas burbujitas del aceite al cocinarse, si ves que las burbujas son grandes, reduce el fuego y, si no hay movimiento de burbujas de aceite, sube un poco el fuego.'),
('Bacalao al pil-pil', 5, 'Retira los lomos de bacalao del aceite y reserva en un plato envuelto con papel de aluminio para que el bacalao no se enfríe. Deja entibiar el aceite que queda en la olla, puedes cambiarlo a otra olla para que el proceso de enfriamiento sea más rápido.'),
('Bacalao al pil-pil', 6, 'Sirve los lomos de bacalao acompañados del aceite emulsionado y los ajos laminados junto con las guindillas. Si se te han enfriado los lomos de bacalao, caliéntalos ligeramente. Es normal que el aceite emulsionado vuelva a disolverse con el calor de los lomos, pues la emulsión se mantiene en frío y desaparece en caliente. Sirve el bacalao al pil-pil enseguida.'),
('Confit de pato con mermelada de naranja',	1, 'Retira la grasa de los de los muslos.'),
('Confit de pato con mermelada de naranja',	2, 'Colócalos sobre la placa de horno y cocínalos a 200ºC durante 15 minutos.'),
('Confit de pato con mermelada de naranja',	3, 'Corta los pimientos (rojo y verde) en juliana fina y ralla las patatas. Sazona y mezcla bien.'),
('Confit de pato con mermelada de naranja',	4, 'Pon un poco de grasa de pato en una sartén, cuando se funda.'),
('Confit de pato con mermelada de naranja',	5, 'Coloca una cucharada de la mezcla de patatas y pimientos, extiéndela hasta formar una torta y fríela por los dos lados.'),
('Confit de pato con mermelada de naranja',	6, 'Calienta la mermelada de naranja en una cazuela.'),
('Confit de pato con mermelada de naranja',	7, 'Sirve en una fuente las tortas, coloca encima los muslos y riégalos con la mermelada.'),
('Confit de pato con mermelada de naranja',	8, 'Adorna la fuente con mermelada y perejil.'),
('Confit de pato con mermelada de naranja',	9, 'Cuando cocinéis confit de pato, lo mejor es utilizar cocciones largas para que se derrita lo más posible la abundante grasa que tienen debajo de la piel y para que las piezas queden bien doradas.'),
('Confit de pato con mermelada de naranja',	10, 'Conviene asimismo bañarlas con frecuencia con la grasa durante la cocción lo que contribuye a que la piel quede crujiente.'),
('Costilla de ternera asada', 1, 'Pon el horno a calentar.'),
('Costilla de ternera asada', 2, 'Corta la costilla en 4 pedazos y colócala sobre la bandeja de hornear.'),
('Costilla de ternera asada', 3, 'En un bol pequeño coloca el romero y el tomillo bien picado, añade aceite y sal y bate todo bien.'),
('Costilla de ternera asada', 4, 'Con una brocha unta las costillas.'),
('Costilla de ternera asada', 5, 'Introduce en el horno a 180º C. y deja que se ase durante 1 hora y media.'),
('Costilla de ternera asada', 6, 'Pela y corta las patatas en medias lunas.'),
('Costilla de ternera asada', 7, 'Cuando estén casi hechas añade las guindillas y deja que se frían juntas.'),
('Costilla de ternera asada', 8, 'Sirve las costillas en una fuente y acompaña de las patatas y las guindillas.'),
('Costilla de ternera asada', 9, 'Sazona todo y listo.'),
('Estofado de judías con tomate y cebolla',	1, 'En una olla calienta el aceite y sofríe la cebolla picada en trozos pequeños hasta que se ponga transparente.'),
('Estofado de judías con tomate y cebolla',	2, 'Incorpora el tomate triturado a la olla'),
('Estofado de judías con tomate y cebolla',	3, 'llévalo a ebullición y cocina durante 5 minutos removiendo de vez en cuando.'),
('Estofado de judías con tomate y cebolla',	4, 'Añade las judías a la olla y cúbrelas con el caldo'),
('Estofado de judías con tomate y cebolla',	5, 'salpimienta y añade la hoja de laurel. Cocina durante 15-20 minutos hasta que la salsa se reduzca.'),
('Estofado de judías con tomate y cebolla',	6, 'Sirve el estofado de judías con tomate y cebolla acompañado de arroz o cualquier otra guarnición que más te guste.'),
('Lentejas viudas', 1, 'Lava bien las lentejas por si tienen algunas piedrecitas o alguna lenteja mala. Escurre y reserva.'),
('Lentejas viudas',	2, 'Prepara las verduras. Para ello, corta en rodajas las zanahorias (previamente peladas), pela los ajos y déjalos enteros, corta el pimiento en 2-3 trozos y parte la cebolla por la mitad. Reserva una parte de cebolla y tritura el tomate.'),
('Lentejas viudas',	3, 'Pon las lentejas en una cazuela, media cebolla, las hojas de laurel, los ajos y el pimiento. Hecho esto, cubre todo con agua fría de manera que sobre pase unos dos dedos las lentejas, como se observa en la foto. Calienta la cazuela a fuego fuerte y, cuando empiece a hervir, baja el fuego a medio y deja cocinar.'),
('Lentejas viudas',	4, 'Mientras se van cocinando las lentejas prepara el sofrito. Para hacerlo, pica la media cebolla que tenías reservada y póchala en una sartén con un poco de aceite caliente. Cuando veas que está transparente, añade el tomate triturado y deja que se cocine durante 4-5 minutos. Agrega la cucharada de pimentón dulce, aparta la sartén del fuego y remueve todo. Este sofrito se puede poner en un bol junto con la cebolla que has añadido a las lentejas, algunas zanahorias y el pimiento y triturarlo todo antes de añadirlo a las lentejas, así no se notan los trozos de verduras. Además, esto da mucho sabor al guiso y lleva todas las verduras incorporadas.'),
('Lentejas viudas',	5, 'Añade un poco de agua al sofrito y remuévelo bien. Prueba las lentejas y si ves que ya están casi listas agrega el sofrito, mezcla y deja que se terminen de cocinar las lentejas viudas y que cojan los sabores del sofrito.'),
('Lentejas viudas',	6, 'Prueba de sal, rectifica y listo. El tiempo de cocción total puede variar según la lenteja, nosotros las hemos tenido 45 minutos en olla convencional, pero a veces necesitan un poco más. Lo mejor es ir probando hasta que la lenteja quede tierna. Así mismo, puedes hacer las lentejas viudas en olla rápida teniendo en cuenta que el tiempo aproximado es de 20 minutos. Sirve la receta de lentejas viudas y disfruta de este plato caliente.'),
('Paella Valenciana', 1, 'Limpia y corta el pollo y el conejo en trozos. Luego, sala las piezas al gusto. Por otro lado, cabe destacar que si usas garrofón tierno, podrás añadirlo directamente en el paso correspondiente, pero si es garrofón seco, deberás dejarlo en remojo la noche anterior y cocerlo antes de hacer la paella para que quede tierno.'),
('Paella Valenciana', 2, 'Pon una paella al fuego con un buen chorro de aceite de oliva. Cuando esté caliente, incorpora los trozos de carne y deja que se doren por todos lados a fuego medio-alto.'),
('Paella Valenciana', 3, 'Lava las judías verdes, corta las puntas y trocéalas. Cuando esté la carne dorada,déjala en un lado de la paella y añade las judías verdes. Deja que se cocinen unos minutos, los suficientes para que se doren un poco. En algunas zonas se considera que la paella valenciana original debe llevar caracoles. Sin embargo, al no ser así en todos los lugares, te dejamos decidir si quieres o no añadirlos. En caso afirmativo, deberás agregarlos con las judías, previamente lavados.'),
('Paella Valenciana', 4, 'Si ves que no hay mucho aceite, ve añadiendo. Agrega el tomate natural picado o triturado y remueve, deja que se cocine durante 5 minutos a fuego medio. Incorpora el pimentón dulce y remueve bien. Lo ideal es hacer la paella valenciana con tomate natural, pero si no tienes, puedes usar tomate triturado de bote (preferiblemente natural y no frito).'),
('Paella Valenciana', 5, 'Vierte el agua, pon un 1 litro y medio o algo más porque se irá consumiendo. Añade un poco de sal y deja se cocine la carne 15 minutos más.'),
('Paella Valenciana', 6, 'A mitad de la cocción, agrega los garrofones, si no los consigues frescos pueden ser congelados. Añade el colorante, el azafrán en hebra y la rama de romero, que puedes poner en un lado para luego poder quitarla fácilmente.'),
('Paella Valenciana', 7, 'Pasados los 15 minutos, prueba de sal y vierte un poco más de agua si es necesario. Es un poco difícil de calcular, pero debe haber el doble de agua que de arroz. Cuando el agua empiece a hervir, añade el arroz y repártelo bien para no tocarlo durante la cocción. Deja que se cocine la paella valenciana durante 5-8 minutos a fuego fuerte.'),
('Paella Valenciana', 8, 'Baja a fuego medio y deja que se siga cocinando el arroz unos 10 minutos más. Ten en cuenta que el tiempo de cocción puede variar en función del tipo de arroz y del tipo de fuego. Si te gusta que quede un poco socarrat, es decir, que se pegue un poquito en el fondo, pon el fuego fuerte al final, unos 2-3 minutos antes de que se termine de cocinar.'),
('Paella Valenciana', 9, 'Deja reposar unos 5 minutos y ya tienes tu receta de paella valenciana lista. Como has podido comprobar, los ingredientes de la paella valenciana tradicional pueden variar en función de la temporada y del lugar, por lo que puedes hacer ligeras modificaciones, como añadir caracoles o alcachofas. En cualquier caso, esperamos que te animes a probarla ahora que sabes cómo hacer una paella valenciana y que nos cuentes cómo te ha quedado.'),
('Pizza de patata',	1, 'Antes de comenzar con la base de la pizza de patatas, precalienta el horno a 200 ºC. Pela y lava la patata. Corta en láminas finas, preferiblemente con una mandolina. Si la cortas con un cuchillo, quizás necesites algo más de patata.'),
('Pizza de patata',	2, 'Forra una bandeja redonda para pizzas con papel de horno. Pon las láminas de patatas en la bandeja. Empieza por el centro para terminar en los extremos de la bandeja, superponiendo un poco una encima de la otra alrededor.'),
('Pizza de patata',	3, 'Espolvorea toda la superficie con las cucharadas de parmesano rallado. Hornea siete minutos a 200 ºC con calor arriba y abajo para que la base de la pizza se compacte.'),
('Pizza de patata',	4, 'Saca la pizza de patata del horno y extiende el tomate frito por toda la superficie.'),
('Pizza de patata',	5, 'Pon encima los champiñones laminados. Añade un poco de sal por encima.'),
('Pizza de patata',	6, 'Lamina la bola de mozzarella y esparce por la pizza. Espolvorea con un poco de orégano seco si deseas, también puedes hacer una pizza de patata y romero. Casca el huevo en el centro de la pizza de patata y hornea 12 minutos más a 200 ºC con calor arriba y abajo.'),
('Pizza de patata',	7, 'Saca la pizza de patata y huevo del horno, trocea las lonchas de jamón serrano y reparte por la pizza.'),
('Pizza de patata',	8, 'Sirve la pizza de patata italiana enseguida para consumirla caliente.'),
('Salmón marinado al horno', 1, 'Prepara la marinada para el salmón al horno en primer lugar porque necesita un tiempo de maduración. Para ello coloca todos los ingredientes en un bol y tritúralos a máxima potencia.'),
('Salmón marinado al horno', 2,	'Luego déjalos reposar durante 1 hora en la nevera/frigorífico y después cuélalos por un colador chino o estameña.'),
('Salmón marinado al horno', 3,	'Mientras se madura la marinada prepara los lomos del salmón. Si lo has comprado fresco deberás escamarlo, limpiarlo bien y cortarlo en lomos y, en el caso de tenerlo congelado, deberás descongelarlo previamente. Resérvalos en la nevera.'),
('Salmón marinado al horno', 4,	'Pasada la hora de maduración, coloca los lomos de salmón en el recipiente de la marinada o en una bolsa hermética si lo prefieres todo junto bien sellado, y deja que el pescado se marine durante 1 hora reservándolo en la nevera.'),
('Salmón marinado al horno', 5,	'Una vez está el salmón marinado, escúrrelo bien, sécalo un poco con papel absorbente y colócalo en un recipiente lleno de aceite de girasol. Reserva.'),
('Salmón marinado al horno', 6,	'Precalienta el horno a 150 ºC. Coge una bandeja o fuente apta y coloca en la base un trozo de papel vegetal/sulfurizado.'),
('Salmón marinado al horno', 7,	'Escurre muy bien los lomos de salmón marinados y ponlos encima de la bandeja de manera que no se toquen.'),
('Salmón marinado al horno', 8,	'Cocina el salmón al horno marinado durante unos 15 minutos aproximadamente o hasta que veas que adquiere un tono rosado y se empieza a dorar por arriba. Te cuidado de no pasarte de tiempo de cocción porque entonces puede quedar seco.'),
('Salmón marinado al horno', 9,	'Cuando esté hecho, sirve el salmón marinado al horno acompañado de una buena ensalada de apio y manzana por ejemplo y tendrás un plato completísimo para comer.'),
('Tarta de queso de La Viña', 1, 'Para comenzar con esta tarta de San Sebastián, añade el queso crema en un recipiente y el azúcar. Ve aplastando para facilitar el mezclado.'),
('Tarta de queso de La Viña', 2, 'Añade el resto de ingredientes: huevos, nata y harina. Mezcla con unas varillas eléctricas o manuales hasta que todo quede completamente integrado. Por otro lado, precalienta el horno a 200 ºC con calor arriba y abajo. Humedece una hoja de papel de hornear.'),
('Tarta de queso de La Viña', 3, 'Escurre el papel de hornear y forra el molde con cuidado. Empuja con los dedos el papel para que quede perfectamente forrado en cada esquina. Vierte la mezcla de la tarta La Viña y lleva al horno ya precalentado. Deposita el molde a altura media-baja y hornea por 40 minutos aproximadamente.'),
('Tarta de queso de La Viña', 4, 'Pasado este tiempo, verifica que la tarta de queso al horno ya esté lista pinchando el centro con un palillo. Seguramente, estará cremosa, pero no debe quedar demasiado cuajada o no tendrá ese punto jugoso.'),
('Tarta de queso de La Viña', 5, 'Después, enfría a temperatura ambiente unas horas y lleva al refrigerador un par de horas más.'),
('Tarta de queso de La Viña', 6, 'El resultado salta a la vista, esta es la tarta de queso La Viña original y deliciosa que todos querrán probar. Puedes añadir mermelada de frutos rojos que le irán de cine, pero con un buen café con leche o un zumo natural para los mas jóvenes de la casa, será suficiente para disfrutarla. ¡Lista para comer!.'),
('Tarta de Santiago con chocolate',	1, 'En un bol y con la ayuda de una batidora, mezcla la mantequilla y los huevos. Una vez integrados, agrégale el azúcar y la ralladura de limón. Es necesario batir hasta blanquear la mezcla y lograr una consistencia cremosa.'),
('Tarta de Santiago con chocolate',	2, 'Derrite el chocolate negro al baño de María y, una vez fundido, déjalo entibiar un poco. Procede a agregar el chocolate fundido en forma de hilos en el bol con la anterior preparación, luego colócale el cacao en polvo y mezcla bien hasta que se unifiquen los ingredientes.'),
('Tarta de Santiago con chocolate',	3, 'Al tener una mezcla homogénea incorpora poco a poco la almendra molida y la canela y mezcla con una espátula haciendo movimientos envolventes.'),
('Tarta de Santiago con chocolate',	4, 'En un molde engrasado y enharinado, vacía la mezcla de manera uniforme. Seguidamente, lleva la tarta de Santiago con chocolate al horno previamente precalentado a unos 160 ºC y hornea por aproximadamente unos 25 minutos o hasta comprobar su cocimiento al introducir un palillo y que salga libre de mezcla.'),
('Tarta de Santiago con chocolate',	5, 'Una vez lista la tarta, déjala enfriar por unos 15 o 20 minutos para desmoldarla y espolvorear cacao en polvo para decorar o azúcar glas.');

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "recetas_creadas"
--

INSERT INTO "recetas_creadas" ("nombre_receta", "email_usuario") VALUES
('Bacalao al pil-pil','maria@recetas.es'),
('Confit de pato con mermelada de naranja','admin@admin.es'),
('Estofado de judías con tomate y cebolla','admin@admin.es');

-- --------------------------------------------------------

--
-- Volcado de datos para la tabla "recetas_favoritas"
--

INSERT INTO "recetas_favoritas" ("nombre_receta", "email_usuario") VALUES
('Bacalao al pil-pil', 'admin@admin.es'),
('Bacalao al pil-pil', 'maria@recetas.es'),
('Bacalao al pil-pil', 'usuario3@recetas.es'),
('Costilla de ternera asada', 'admin@admin.es'),
('Costilla de ternera asada', 'prueba1@p.es'),
('Estofado de judías con tomate y cebolla', 'admin@admin.es'),
('Lentejas viudas',	'admin@admin.es'),
('Lentejas viudas',	'maria@recetas.es'),
('Lentejas viudas',	'usuario3@recetas.es'),
('Tarta de queso de La Viña', 'admin@admin.es');









