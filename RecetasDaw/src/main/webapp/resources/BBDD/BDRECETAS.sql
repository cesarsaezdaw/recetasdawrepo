--
-- Estructura de tabla para la tabla "RECETAS"
--

DROP TABLE IF EXISTS "recetas";

CREATE TABLE "recetas" (
  "nombre" varchar(50) NOT NULL PRIMARY KEY,
  "descripcion" varchar(10000) NOT NULL,
  "notas" varchar(10000),
  "pais" varchar(20) NOT NULL,
  "tipo_alimento" varchar(20) NOT NULL,
  "duracion" varchar(20) NOT NULL,
  "numero_personas" integer NOT NULL,
  "foto" varchar(50) NOT NULL,
  "visualizaciones" integer NOT NULL
);

--
-- Estructura de tabla para la tabla "USUARIOS"
--

DROP TABLE IF EXISTS "usuarios";

CREATE TABLE "usuarios" (
  "email" varchar(40) NOT NULL PRIMARY KEY,
  "password" varchar(20) NOT NULL,
  "nombre" varchar (20),
  "apellidos" varchar(40),
  "administrador" boolean NOT NULL  
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla "RECETAS_FAVORITAS"
--

DROP TABLE IF EXISTS "recetas_favoritas";

CREATE TABLE "recetas_favoritas" (
  "nombre_receta" varchar(50) NOT NULL,
  "email_usuario" varchar(40) NOT NULL, 
  PRIMARY KEY ("nombre_receta", "email_usuario")
); 

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla "PAISES"
--

DROP TABLE IF EXISTS "paises";

CREATE TABLE "paises" (
  "nombre" varchar(15) NOT NULL PRIMARY KEY
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla "TIPO_ALIMENTO"
--

DROP TABLE IF EXISTS "tipos_alimento";

CREATE TABLE "tipos_alimento" (
  "nombre" varchar(15) NOT NULL PRIMARY KEY
); 

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla "RECETAS_CREADAS"
--

DROP TABLE IF EXISTS "recetas_creadas";

CREATE TABLE "recetas_creadas" (
  "nombre_receta" varchar(50) NOT NULL,
  "email_usuario" varchar(40) NOT NULL, 
  PRIMARY KEY ("nombre_receta", "email_usuario")
); 

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla "PASOS_RECETAS"
--

DROP TABLE IF EXISTS "pasos_recetas";

CREATE TABLE "pasos_recetas" (
  "nombre_receta" varchar(100) NOT NULL,
  "numero_paso" integer NOT NULL, 
  "descripcion" varchar(10000) NOT NULL,
  PRIMARY KEY ("nombre_receta", "numero_paso")
); 