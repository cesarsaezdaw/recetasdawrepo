package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.Pais;

public interface PaisDAOInterface {
	
	public List<Pais> findAll();
	
	public Pais findById(String nombre);
	
	public void insert(Pais pais);
}
