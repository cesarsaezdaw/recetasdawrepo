package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recetas_creadas")
public class MiRecetaCreada implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nombre_receta")
	private String nombreReceta;
		
	@Id
	@Column(name="email_usuario")
	private String emailUsuario;
	
	public MiRecetaCreada() {
		super();
	}

	public MiRecetaCreada(String nombreReceta, String emailUsuario) {
		super();
		this.nombreReceta = nombreReceta;
		this.emailUsuario = emailUsuario;
	}

	public String getNombreReceta() {
		return nombreReceta;
	}

	public void setNombreReceta(String nombreReceta) {
		this.nombreReceta = nombreReceta;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	@Override
	public String toString() {
		return "MiRecetaCreada [nombreReceta=" + nombreReceta + ", emailUsuario=" + emailUsuario + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(emailUsuario, nombreReceta);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MiRecetaCreada other = (MiRecetaCreada) obj;
		return Objects.equals(emailUsuario, other.emailUsuario) && Objects.equals(nombreReceta, other.nombreReceta);
	}

}
