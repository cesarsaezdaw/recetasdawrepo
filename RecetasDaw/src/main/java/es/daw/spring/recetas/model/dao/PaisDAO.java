package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.Pais;

@Repository
public class PaisDAO implements PaisDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Pais> findAll() {
		
		return (List<Pais>) em.createQuery("from Pais").getResultList();		

	}
	
	@Transactional
	@Override
	public Pais findById(String nombre) {
		
		return (Pais) em.createQuery("from Pais as r where r.nombre = '" + nombre + "'").getSingleResult();
	}

	@Transactional
	@Override
	public void insert(Pais pais) {

		this.em.persist(pais);
		
	}
	
}
