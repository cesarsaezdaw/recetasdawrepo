package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.RecetaFavorita;

public interface RecetasFavoritasDAOInterface {
	
	public List<RecetaFavorita> findAll();
	
	public List<RecetaFavorita> findAll(String nombreReceta);
	
	public List<RecetaFavorita> findAllByUsuario(String usuario);
	
	public RecetaFavorita findById(String nombreReceta, String usuario);

	public void insertFavorita(String nombreReceta, String usuario);

	public void deleteFavorita(String nombreReceta, String usuario);
	
}
