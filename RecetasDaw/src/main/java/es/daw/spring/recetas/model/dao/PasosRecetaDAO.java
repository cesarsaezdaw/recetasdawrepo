package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.PasoReceta;

@Repository
public class PasosRecetaDAO implements PasosRecetaDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<PasoReceta> findAll() {
		
		return (List<PasoReceta>) em.createQuery("from PasoReceta").getResultList();		

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<PasoReceta> findAll(String nombreReceta) {
		
		return (List<PasoReceta>) em.createQuery("from PasoReceta as r where r.nombreReceta = '" + nombreReceta + "'").getResultList();		

	}
	
	@Transactional
	@Override
	public PasoReceta findById(String nombreReceta, Integer numPaso) {
					
		return (PasoReceta) em.createQuery("from PasoReceta as r where r.nombreReceta = '" + 
		                        nombreReceta + "' and r.numPaso = '" + numPaso + "'").getSingleResult();

	}
	
	@Transactional
	@Override
	public void insertPasoReceta(PasoReceta pasoReceta) {
				
		this.em.persist(pasoReceta);
		
	}

	@Transactional
	@Override
	public void deletePasoReceta(PasoReceta pasoReceta) {
		
		this.em.remove(pasoReceta);
		
	}
	
	@Transactional
	@Override
	public void deletePasos(String nombreReceta) {
		
		List<PasoReceta> pasos =  this.findAll(nombreReceta);
		
		for(PasoReceta p : pasos) {
			this.em.remove(p);
		}
			
	}

}
