package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.TipoAlimento;

@Repository
public class TipoAlimentoDAO implements TipoAlimentoDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<TipoAlimento> findAll() {
		
		return (List<TipoAlimento>) em.createQuery("from TipoAlimento").getResultList();		

	}
	
	@Transactional
	@Override
	public TipoAlimento findById(String nombre) {
		
		return (TipoAlimento) em.createQuery("from TipoAlimento as r where r.nombre = '" + nombre + "'").getSingleResult();
	}

	@Transactional
	@Override
	public void insert(TipoAlimento tipoAlimento) {

		this.em.persist(tipoAlimento);
		
	}
	
}
