package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="paises")
public class Pais implements Serializable{

	private static final long serialVersionUID = 1L;

	public Pais() {}
	
	public Pais(String nombre) {
		super();
		this.nombre = nombre;		
	}

	@Id
	@Column(name="nombre")
	private String nombre;	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		return Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Pais [nombre=" + nombre + "]";
	}
}
