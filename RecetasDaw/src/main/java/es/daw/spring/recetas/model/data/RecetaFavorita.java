package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recetas_favoritas")
public class RecetaFavorita implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nombre_receta")
	private String nombreReceta;
		
	@Id
	@Column(name="email_usuario")
	private String emailUsuario;
	
	public RecetaFavorita() {
		super();
	}

	public RecetaFavorita(String nombreReceta, String emailUsuario) {
		super();
		this.nombreReceta = nombreReceta;
		this.emailUsuario = emailUsuario;
	}

	public String getNombreReceta() {
		return nombreReceta;
	}

	public void setNombreReceta(String nombreReceta) {
		this.nombreReceta = nombreReceta;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	@Override
	public String toString() {
		return "RecetaFavorita [nombreReceta=" + nombreReceta + ", emailUsuario=" + emailUsuario + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(emailUsuario, nombreReceta);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecetaFavorita other = (RecetaFavorita) obj;
		return Objects.equals(emailUsuario, other.emailUsuario) && Objects.equals(nombreReceta, other.nombreReceta);
	}

}
