package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pasos_recetas")
public class PasoReceta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nombre_receta")
	private String nombreReceta;
	public String getNombreReceta() { return nombreReceta;}
	public void setNombreReceta(String nombreReceta) {this.nombreReceta = nombreReceta;}
	
	@Id
	@Column(name="numero_paso")
	private Integer numPaso;
	public Integer getNumPaso() { return numPaso;}
	public void setNumPaso(Integer numPaso) {this.numPaso = numPaso;}
	
	@Column(name="descripcion")
	private String descripcion;
	public String getDescripcion() { return descripcion;}
	public void setDescripcion(String descripcion) {this.descripcion = descripcion;}
		
	public PasoReceta(String nombreReceta, Integer numPaso, String descripcion) {
		super();
		this.nombreReceta = nombreReceta;
		this.numPaso = numPaso;
		this.descripcion = descripcion;
	}
	
	public PasoReceta() {}
	
	@Override
	public String toString() {
		return "PasoReceta [nombreReceta=" + nombreReceta + ", numPaso=" + numPaso + ", descripcion=" + descripcion
				+ "]";
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(descripcion, nombreReceta, numPaso);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PasoReceta other = (PasoReceta) obj;
		return Objects.equals(descripcion, other.descripcion) && Objects.equals(nombreReceta, other.nombreReceta)
				&& Objects.equals(numPaso, other.numPaso);
	};
	
}
