package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.PasoReceta;

public interface PasosRecetaDAOInterface {
	
	public List<PasoReceta> findAll();
	
	public List<PasoReceta> findAll(String nombreReceta);
	
	public PasoReceta findById(String nombreReceta, Integer numPaso);

	public void insertPasoReceta(PasoReceta pasoReceta);

	public void deletePasoReceta(PasoReceta pasoReceta);
	
	public void deletePasos(String nombreReceta);
	
}
