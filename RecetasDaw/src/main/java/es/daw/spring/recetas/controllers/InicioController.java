package es.daw.spring.recetas.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.daw.spring.recetas.model.dao.RecetasDAOInterface;

@Controller
public class InicioController {

	@Autowired RecetasDAOInterface recetasDAO;
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String index(Model model, HttpSession session) {		
		
		//SESIÓN
		if(session.getAttribute("login") != null) {
			String login = session.getAttribute("login").toString();
			model.addAttribute("login", login);
		}else {
			String login = "";
			model.addAttribute("login", login);
		}
		
		//Listado de recetas
		model.addAttribute("recetas", this.recetasDAO.findAll());	
		
		return "index";
	}

}
