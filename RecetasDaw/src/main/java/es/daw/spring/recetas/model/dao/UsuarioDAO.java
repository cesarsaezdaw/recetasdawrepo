package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.Usuario;

@Repository
public class UsuarioDAO implements UsuarioDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Usuario> findAll() {
		
		return (List<Usuario>) em.createQuery("from Usuario as u order by u.email").getResultList();		

	}
	
	@Transactional
	@Override
	public Usuario findById(String email) {
		
		return (Usuario) em.createQuery("from Usuario as u where u.email = '" + email + "'").getSingleResult();
	}

	@Transactional
	@Override
	public void insert(Usuario usuario) {

		this.em.persist(usuario);
		
	}
	
	@Transactional
	public void update(Usuario usuario) {

		em.createQuery("update Usuario as u set "
				+ "u.password = '" + usuario.getPassword() + "' "
				+ ",u.nombre = '" + usuario.getNombre() + "' "
				+ ",u.apellidos = '" + usuario.getApellidos() + "' "
				+ " where u.email = '" + usuario.getEmail() + "'");		
				
	}	
	
	@Transactional
	@Override
	public void delete(Usuario usuario) {

		this.em.remove(usuario);
		
	}
	
}
