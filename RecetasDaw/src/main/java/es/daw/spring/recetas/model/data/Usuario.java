package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;

	public Usuario() {}
	
	public Usuario(String email, String password, String nombre, String apellidos, Boolean administrador) {
		super();
		this.email = email;
		this.password = password;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.administrador = administrador;
	}

	@Id
	@Column(name="email")
	private String email;	
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}

	@Column(name="password")
	private String password;	
	public String getPassword() {return password;}
	public void setPassword(String password) {this.password = password;}
	
	@Column(name="nombre")
	private String nombre;	
	public String getNombre() {return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
		
	@Column(name="apellidos")
	private String apellidos;	
	public String getApellidos() {return apellidos;}
	public void setApellidos(String apellidos) {this.apellidos = apellidos;}
	
	@Column(name="administrador")
	private Boolean administrador;	
	public Boolean getAdministrador() {return administrador;}
	public void setAdministrador(Boolean administrador) {this.administrador = administrador;}

	@Override
	public int hashCode() {
		return Objects.hash(administrador, apellidos, email, nombre, password);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		return Objects.equals(administrador, other.administrador) && Objects.equals(apellidos, other.apellidos)
				&& Objects.equals(email, other.email) && Objects.equals(nombre, other.nombre)
				&& Objects.equals(password, other.password);
	}

	@Override
	public String toString() {
		return "Usuario [email=" + email + ", password=" + password + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", administrador=" + administrador + "]";
	}
	
}
