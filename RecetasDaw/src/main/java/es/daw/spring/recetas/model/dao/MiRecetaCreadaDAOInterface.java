package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.MiRecetaCreada;

public interface MiRecetaCreadaDAOInterface {
	
	public List<MiRecetaCreada> findAll();
	
	public List<MiRecetaCreada> findAll(String usuario);
	
	public MiRecetaCreada findByNombreReceta(String nombreReceta);
	
	public MiRecetaCreada findById(String nombreReceta, String usuario);

	public void insertMiReceta(String nombreReceta, String usuario);

	public void deleteMiReceta(String nombreReceta, String usuario);
	
	public void deleteRecetaCreada(String nombreReceta);
	
}
