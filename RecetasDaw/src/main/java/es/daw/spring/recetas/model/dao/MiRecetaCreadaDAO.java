package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.MiRecetaCreada;

@Repository
public class MiRecetaCreadaDAO implements MiRecetaCreadaDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<MiRecetaCreada> findAll() {
		
		return (List<MiRecetaCreada>) em.createQuery("from MiRecetaCreada").getResultList();		

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<MiRecetaCreada> findAll(String usuario) {
		
		return (List<MiRecetaCreada>) em.createQuery("from MiRecetaCreada as r where r.emailUsuario = '" + usuario + "'").getResultList();		

	}
	
	@Transactional
	@Override
	public MiRecetaCreada findByNombreReceta(String nombreReceta) {
		
		return  (MiRecetaCreada) em.createQuery("from MiRecetaCreada as r where r.nombreReceta = '" + nombreReceta + "'").getSingleResult();	

	}
	
	@Transactional
	@Override
	public MiRecetaCreada findById(String nombreReceta, String usuario) {
					
		return (MiRecetaCreada) em.createQuery("from MiRecetaCreada as r where r.nombreReceta = '" + 
		                        nombreReceta + "' and r.emailUsuario = '" + usuario + "'").getSingleResult();

	}
	
	@Transactional
	@Override
	public void insertMiReceta(String nombreReceta, String usuario) {

		MiRecetaCreada miReceta = new MiRecetaCreada(nombreReceta,usuario);
				
		this.em.persist(miReceta);
	}

	@Transactional
	@Override
	public void deleteMiReceta(String nombreReceta, String usuario) {
		
		MiRecetaCreada miReceta = findById(nombreReceta,usuario);
		
		this.em.remove(miReceta);
		
	}
	@Transactional
	@Override
	public void deleteRecetaCreada(String nombreReceta) {
		
		MiRecetaCreada recetaCreada = findByNombreReceta(nombreReceta);
		
		this.em.remove(recetaCreada);
		
	}
	
}
