package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.RecetaFavorita;

@Repository
public class RecetasFavoritasDAO implements RecetasFavoritasDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<RecetaFavorita> findAll() {
		
		return (List<RecetaFavorita>) em.createQuery("from RecetaFavorita").getResultList();		

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<RecetaFavorita> findAll(String nombreReceta) {
		
		return (List<RecetaFavorita>) em.createQuery("from RecetaFavorita as r where r.nombreReceta = '" +
								nombreReceta + "'").getResultList();		

	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<RecetaFavorita> findAllByUsuario(String usuario) {
		
		return (List<RecetaFavorita>) em.createQuery("from RecetaFavorita as r where r.emailUsuario = '" +
				usuario + "'").getResultList();		

	}
	
	@Transactional
	@Override
	public RecetaFavorita findById(String nombreReceta, String usuario) {
					
		return (RecetaFavorita) em.createQuery("from RecetaFavorita as r where r.nombreReceta = '" + 
		                        nombreReceta + "' and r.emailUsuario = '" + usuario + "'").getSingleResult();

	}
	
	@Transactional
	@Override
	public void insertFavorita(String nombreReceta, String usuario) {

		RecetaFavorita favorita = new RecetaFavorita(nombreReceta,usuario);
				
		this.em.persist(favorita);
	}

	@Transactional
	@Override
	public void deleteFavorita(String nombreReceta, String usuario) {
		
		RecetaFavorita favorita = findById(nombreReceta,usuario);
		
		this.em.remove(favorita);
		
	}

}
