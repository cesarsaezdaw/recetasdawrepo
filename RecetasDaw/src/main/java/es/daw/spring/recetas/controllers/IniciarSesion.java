package es.daw.spring.recetas.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.daw.spring.recetas.model.dao.PaisDAO;
import es.daw.spring.recetas.model.dao.PasosRecetaDAO;
import es.daw.spring.recetas.model.dao.RecetasDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasFavoritasDAOInterface;
import es.daw.spring.recetas.model.dao.TipoAlimentoDAO;
import es.daw.spring.recetas.model.dao.UsuarioDAO;
import es.daw.spring.recetas.model.data.Receta;
import es.daw.spring.recetas.model.data.Usuario;
import es.daw.spring.recetas.utils.FormLogin;


@Controller
public class IniciarSesion {

	@Autowired RecetasDAOInterface recetasDAO;
	@Autowired RecetasFavoritasDAOInterface recetasFavoritasDAO;
	@Autowired PaisDAO paisDAO;
	@Autowired TipoAlimentoDAO tipoAlimentoDAO;
	@Autowired PasosRecetaDAO pasosRecetaDAO;
	@Autowired UsuarioDAO usuarioDAO;
	@Autowired InicioController inicioController;
		
	@RequestMapping(value="/iniciarSesion.ctr", method=RequestMethod.GET)
	public String iniciarSesion(Model model, HttpSession session) {	
				
		String login = "";
		model.addAttribute("login",login);
		session.setAttribute("login",login);
		
		return "iniciarSesion";
	}	
	
	@RequestMapping(value="/login.ctr", method=RequestMethod.POST)	
	public String login(FormLogin formLogin, Model model, HttpSession session) {	
		
		String email = formLogin.getEmail();
		String password = formLogin.getPassword();		
		String login = "noUser";
		
		try {			
			Usuario usuario = this.usuarioDAO.findById(email);
			
			if (usuario.getPassword().equals(password)) {
				//OK
				if(usuario.getAdministrador()) {
					login = "admin";
				}else {
					login = "user";
				}			
				session.setAttribute("usuario",usuario);
				model.addAttribute("usuario",usuario);
			}else {
				//KO
				login = "noUser";
			}				
		}catch(Exception e) {			
			login = "noUser";			
		}				
		model.addAttribute("login",login);
		session.setAttribute("login",login);
		
		if(login.equals("noUser")) {
			return "iniciarSesion";
		}else {
			return this.inicioController.index(model, session);
		}
					
	}
	
	@RequestMapping(value="/cerrarSesion.ctr", method=RequestMethod.GET)	
	public String cerrarSesion(FormLogin formLogin, Model model, HttpSession session) {	
		
		session.invalidate();		
		Boolean sesionCerrada = true;		
		model.addAttribute("sesionCerrada",sesionCerrada);
		model.addAttribute("login","noUser");
		
		List<Receta> recetas = recetasDAO.findAll();
	    model.addAttribute("recetas", recetas);
	    
		return "index";//jsp
	}
	
	@RequestMapping(value="/registro.ctr", method=RequestMethod.GET)	
	public String registro(Model model, HttpSession session) {	
			
		//Lista de correos de usuario para verificar en cliente si es repetido
		List<String> listEmail = new ArrayList<>();
		
		List<Usuario> usuarios = this.usuarioDAO.findAll();
		for(Usuario usuario : usuarios) {
			listEmail.add(usuario.getEmail());
		}
		
		model.addAttribute("listEmail", listEmail);
		
		return "registro";//jsp
	}
	
	@RequestMapping(value="/registrarUsuario.ctr", method=RequestMethod.POST)	
	public String registrarUsuario(FormLogin formLogin, Model model, HttpSession session) {	
			
		Usuario usuario = new Usuario();		
		usuario.setEmail(formLogin.getEmail());
		usuario.setPassword(formLogin.getPassword());
		usuario.setNombre(formLogin.getNombre());
		usuario.setApellidos(formLogin.getApellidos());		
		usuario.setAdministrador(false);//por defecto nunca es administrador
		
		Boolean nuevoRegistro = null;
		try {			
			this.usuarioDAO.insert(usuario);
			nuevoRegistro = true;
			session.setAttribute("usuario", usuario);
			session.setAttribute("login", "user");
			model.addAttribute("nuevoRegistro", nuevoRegistro);
			
			return this.inicioController.index(model, session);
			
		}catch(Exception e) {
			nuevoRegistro = false;
			model.addAttribute("nuevoRegistro", nuevoRegistro);
			
			return "registro";//jsp
		}

	}
	
	@RequestMapping(value="/perfilUsuario.ctr", method=RequestMethod.GET)	
	public String perfilUsuario(FormLogin formLogin, Model model, HttpSession session) {	
				
		Usuario usuario = (Usuario) session.getAttribute("usuario");		
		model.addAttribute("usuario", usuario);
		Boolean modificado = null;
		model.addAttribute("modificado", modificado);		

		return "perfilUsuario";
	}
	
	@RequestMapping(value="/modificarPerfilUsuario.ctr", method=RequestMethod.POST)	
	public String modificarPerfilUsuario(FormLogin formLogin, Model model, HttpSession session) {	
				
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		String email = usuario.getEmail();
		boolean administrador = usuario.getAdministrador();
		Boolean modificado = null;
		try {		
			
			usuario = this.usuarioDAO.findById(email);
			this.usuarioDAO.delete(usuario);
			Usuario user = new Usuario();
			user.setEmail(email);
			user.setPassword(formLogin.getPassword());
			user.setNombre(formLogin.getNombre());
			user.setApellidos(formLogin.getApellidos());	
			user.setAdministrador(administrador);
			
			this.usuarioDAO.insert(user);
			modificado = true;
			model.addAttribute("modificado", modificado);
			model.addAttribute("usuario", user);
			
		}catch(Exception e) {
			modificado=false;
			model.addAttribute("modificado", modificado);
		}
				
		return "perfilUsuario";
	}
	
	@RequestMapping(value="/gestionUsuarios.ctr", method=RequestMethod.GET)	
	public String gestionarUsuarios(Model model, HttpSession session) {
		
		List<Usuario> usuarios = this.usuarioDAO.findAll();		
		model.addAttribute("usuarios", usuarios);	
		
		return "gestionUsuarios";
	}
	
	@RequestMapping(value="/cambioAdministracionUsuario.ctr", method=RequestMethod.POST)	
	public String cambioAdministracionUsuario(FormLogin formLogin, Model model, HttpSession session) {	
		
		String email = formLogin.getEmail();
		Usuario usuario = this.usuarioDAO.findById(email);
		Boolean administrador = usuario.getAdministrador();
		String login;
		if(administrador) {
			Usuario usuarioSesion = (Usuario) session.getAttribute("usuario");
			usuario.setAdministrador(false);
			if(!usuarioSesion.getEmail().equals("admin@admin.es")) {
				login = "user";				
				model.addAttribute("login",login);
				usuario.setAdministrador(false);
				this.usuarioDAO.update(usuario);
				session.setAttribute("usuario",usuario);
				session.setAttribute("login",login);
						
				return this.inicioController.index(model, session);
			}
		}else {
			usuario.setAdministrador(true);
		}
		
		Boolean cambioAdministracionUsuario = false;
		try {
			this.usuarioDAO.update(usuario);
			cambioAdministracionUsuario = true;		
		}catch(Exception e) {
			cambioAdministracionUsuario = false;			
		}
		model.addAttribute("cambioAdministracionUsuario", cambioAdministracionUsuario);
		
		//actualizo lista
		List<Usuario> usuarios = this.usuarioDAO.findAll();
		model.addAttribute("usuarios", usuarios);
						
		return "gestionUsuarios";
	}
	
	@RequestMapping(value="/eliminarUsuario.ctr", method=RequestMethod.POST)	
	public String eliminarUsuario(FormLogin formLogin, Model model, HttpSession session) {	
		
		Usuario usuarioSesion = (Usuario) session.getAttribute("usuario");	
	
		String emailUsuario = formLogin.getEmail();
		Usuario usuario = this.usuarioDAO.findById(emailUsuario);		
		Boolean eliminadoUsuario =  null;
		
		//Usuario a borrar es el mismo de la sesion 
		if(usuarioSesion.equals(usuario)) {
			try {
				this.usuarioDAO.delete(usuario);
				eliminadoUsuario = true;
				session.invalidate();
			}catch(Exception e){
				eliminadoUsuario = false;
			}	
			model.addAttribute("eliminadoUsuario", eliminadoUsuario);
			
			//Listado de recetas
			model.addAttribute("recetas", this.recetasDAO.findAll());
			
			return "registro";
			
		}else { //usuario a eliminar es diferente a usuario de la sesión (administrador)
			try {
				this.usuarioDAO.delete(usuario);
				eliminadoUsuario = true;
			}catch(Exception e){
				eliminadoUsuario = false;
			}	
			
			model.addAttribute("eliminadoUsuario", eliminadoUsuario);
			
			//actualizo lista
			List<Usuario> usuarios = this.usuarioDAO.findAll();
			model.addAttribute("usuarios", usuarios);
			
			return "gestionUsuarios";
		}
		
	}

}
