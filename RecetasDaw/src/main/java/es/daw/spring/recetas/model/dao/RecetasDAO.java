package es.daw.spring.recetas.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.daw.spring.recetas.model.data.Receta;

@Repository
public class RecetasDAO implements RecetasDAOInterface {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Receta> findAll() {
		
		return (List<Receta>) em.createQuery("from Receta as r order by r.visualizaciones desc").getResultList();		

	}
	
	@Transactional
	@Override
	public Receta findById(String nombreReceta) {
		
		return (Receta) em.createQuery("from Receta as r where r.nombre = '" + nombreReceta + "'").getSingleResult();
	}

	@Transactional
	@Override
	public void insert(Receta receta) {

		this.em.persist(receta);
		
	}
	
	@Transactional
	@Override
	public void deleteById(String nombreReceta) {
		
		Receta receta = this.findById(nombreReceta);
		
		this.em.remove(receta);
		
	}
	
	@Transactional
	@Override
	public void delete(Receta receta) {

		this.em.remove(receta);
		
	}
	
	@Transactional
	public void update(Receta receta) {
		
		em.createQuery("update Receta as r set "
				+ "r.descripcion = '" + receta.getDescripcion() + "' "
				+ ",r.notas = '" + receta.getNotas() + "' "
				+ ",r.duracion = '" + receta.getDuracion() + "' "
				+ ",r.pais = '" + receta.getPais() + "' "
				+ ",r.tipoAlimento = '" + receta.getTipoAlimento() + "' "
				+ ",r.numeroPersonas = '" + receta.getNumeroPersonas() + "' "				
				+ " where r.nombre = '" + receta.getNombre() + "'");		
	}
	
}
