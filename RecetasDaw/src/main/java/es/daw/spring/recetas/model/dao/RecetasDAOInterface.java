package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.Receta;

public interface RecetasDAOInterface {
	
	public List<Receta> findAll();
	
	public Receta findById(String nombreReceta);
	
	public void insert(Receta receta);
	
	public void deleteById(String nombreReceta);
	
	public void delete(Receta receta);
	
	public void update(Receta receta);
	
}
