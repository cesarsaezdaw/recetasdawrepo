package es.daw.spring.recetas.utils;

import org.springframework.web.multipart.MultipartFile;

public class FormRecetas {

	private String nombreReceta;
	private String descripcionReceta;
	//private String [] preparacionReceta;
	private String notasReceta;
	private String paisReceta;
	private String tipoAlimentoReceta;
	private String duracionReceta;
	private Integer numeroPersonasReceta;
	private MultipartFile fotoReceta;
	private String preparacionRecetaPaso1;
	private String preparacionRecetaPaso2;
	private String preparacionRecetaPaso3;
	private String preparacionRecetaPaso4;
	private String preparacionRecetaPaso5;
	private String preparacionRecetaPaso6;
	private String preparacionRecetaPaso7;
	private String preparacionRecetaPaso8;
	private String preparacionRecetaPaso9;
	private String preparacionRecetaPaso10;


	public String getNombreReceta() {
		return nombreReceta;
	}

	public void setNombreReceta(String nombreReceta) {
		this.nombreReceta = nombreReceta;
	}

	public String getDescripcionReceta() {
		return descripcionReceta;
	}

	public void setDescripcionReceta(String descripcionReceta) {
		this.descripcionReceta = descripcionReceta;
	}
/*
	public String [] getPreparacionReceta() {
		return preparacionReceta;
	}

	public void setPreparacionReceta(String [] preparacionReceta) {
		this.preparacionReceta = preparacionReceta;
	}
*/
	public String getNotasReceta() {
		return notasReceta;
	}

	public void setNotasReceta(String notasReceta) {
		this.notasReceta = notasReceta;
	}

	public String getPaisReceta() {
		return paisReceta;
	}

	public void setPaisReceta(String paisReceta) {
		this.paisReceta = paisReceta;
	}

	public String getTipoAlimentoReceta() {
		return tipoAlimentoReceta;
	}

	public void setTipoAlimentoReceta(String tipoAlimentoReceta) {
		this.tipoAlimentoReceta = tipoAlimentoReceta;
	}

	public String getDuracionReceta() {
		return duracionReceta;
	}

	public void setDuracionReceta(String duracionReceta) {
		this.duracionReceta = duracionReceta;
	}

	public Integer getNumeroPersonasReceta() {
		return numeroPersonasReceta;
	}

	public void setNumeroPersonasReceta(Integer numeroPersonasReceta) {
		this.numeroPersonasReceta = numeroPersonasReceta;
	}

	public MultipartFile getFotoReceta() {
		return fotoReceta;
	}

	public void setFotoReceta(MultipartFile fotoReceta) {
		this.fotoReceta = fotoReceta;
	}

	public String getPreparacionRecetaPaso1() {
		return preparacionRecetaPaso1;
	}

	public void setPreparacionRecetaPaso1(String preparacionRecetaPaso1) {
		this.preparacionRecetaPaso1 = preparacionRecetaPaso1;
	}

	public String getPreparacionRecetaPaso2() {
		return preparacionRecetaPaso2;
	}

	public void setPreparacionRecetaPaso2(String preparacionRecetaPaso2) {
		this.preparacionRecetaPaso2 = preparacionRecetaPaso2;
	}

	public String getPreparacionRecetaPaso3() {
		return preparacionRecetaPaso3;
	}

	public void setPreparacionRecetaPaso3(String preparacionRecetaPaso3) {
		this.preparacionRecetaPaso3 = preparacionRecetaPaso3;
	}

	public String getPreparacionRecetaPaso4() {
		return preparacionRecetaPaso4;
	}

	public void setPreparacionRecetaPaso4(String preparacionRecetaPaso4) {
		this.preparacionRecetaPaso4 = preparacionRecetaPaso4;
	}

	public String getPreparacionRecetaPaso5() {
		return preparacionRecetaPaso5;
	}

	public void setPreparacionRecetaPaso5(String preparacionRecetaPaso5) {
		this.preparacionRecetaPaso5 = preparacionRecetaPaso5;
	}

	public String getPreparacionRecetaPaso6() {
		return preparacionRecetaPaso6;
	}

	public void setPreparacionRecetaPaso6(String preparacionRecetaPaso6) {
		this.preparacionRecetaPaso6 = preparacionRecetaPaso6;
	}

	public String getPreparacionRecetaPaso7() {
		return preparacionRecetaPaso7;
	}

	public void setPreparacionRecetaPaso7(String preparacionRecetaPaso7) {
		this.preparacionRecetaPaso7 = preparacionRecetaPaso7;
	}

	public String getPreparacionRecetaPaso8() {
		return preparacionRecetaPaso8;
	}

	public void setPreparacionRecetaPaso8(String preparacionRecetaPaso8) {
		this.preparacionRecetaPaso8 = preparacionRecetaPaso8;
	}

	public String getPreparacionRecetaPaso9() {
		return preparacionRecetaPaso9;
	}

	public void setPreparacionRecetaPaso9(String preparacionRecetaPaso9) {
		this.preparacionRecetaPaso9 = preparacionRecetaPaso9;
	}

	public String getPreparacionRecetaPaso10() {
		return preparacionRecetaPaso10;
	}

	public void setPreparacionRecetaPaso10(String preparacionRecetaPaso10) {
		this.preparacionRecetaPaso10 = preparacionRecetaPaso10;
	}
	
}
