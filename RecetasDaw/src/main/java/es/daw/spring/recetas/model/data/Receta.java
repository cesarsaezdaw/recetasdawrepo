package es.daw.spring.recetas.model.data;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recetas")
public class Receta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="notas")
	private String notas;
	
	@Column(name="pais")
	private String pais;
	
	@Column(name="tipo_alimento")
	private String tipoAlimento;
	
	@Column(name="duracion")
	private String duracion;
	
	@Column(name="numero_personas")
	private Integer numeroPersonas;
	
	@Column(name="foto")
	private String foto;
	
	@Column(name="visualizaciones")
	private Integer visualizaciones;
	
		
	public Receta() {}
	
	public Receta(String nombre, String descripcion, String notas, String pais,
			String tipoAlimento, String duracion, Integer numeroPersonas, String foto, Integer visualizaciones) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.notas = notas;
		this.pais = pais;
		this.tipoAlimento = tipoAlimento;
		this.duracion = duracion;
		this.numeroPersonas = numeroPersonas;
		this.foto = foto;
		this.visualizaciones = visualizaciones;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNotas() {
		return notas;
	}
	
	public void setNotas(String notas) {
		this.notas = notas;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getTipoAlimento() {
		return tipoAlimento;
	}
	
	public void setTipoAlimento(String tipoAlimento) {
		this.tipoAlimento = tipoAlimento;
	}
	
	public String getDuracion() {
		return duracion;
	}
	
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	
	public Integer getNumeroPersonas() {
		return numeroPersonas;
	}
	
	public void setNumeroPersonas(Integer numeroPersonas) {
		this.numeroPersonas = numeroPersonas;
	}
	
	public String getFoto() {
		return foto;
	}
	
	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public Integer getVisualizaciones() {
		return visualizaciones;
	}

	public void setVisualizaciones(Integer visualizaciones) {
		this.visualizaciones = visualizaciones;
	}

	@Override
	public int hashCode() {
		return Objects.hash(descripcion, duracion, foto, nombre, notas, numeroPersonas, pais, tipoAlimento, visualizaciones);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Receta other = (Receta) obj;
		return Objects.equals(descripcion, other.descripcion) && Objects.equals(duracion, other.duracion)
				&& Objects.equals(foto, other.foto) && Objects.equals(nombre, other.nombre)
				&& Objects.equals(notas, other.notas) && Objects.equals(numeroPersonas, other.numeroPersonas)
				&& Objects.equals(pais, other.pais) && Objects.equals(tipoAlimento, other.tipoAlimento)
				&& Objects.equals(visualizaciones, other.visualizaciones);
	}

	@Override
	public String toString() {
		return "Receta [nombre=" + nombre + ", descripcion=" + descripcion + ", notas=" + notas + ", pais=" + pais
				+ ", tipoAlimento=" + tipoAlimento + ", duracion=" + duracion + ", numeroPersonas=" + numeroPersonas
				+ ", foto=" + foto + ", visualizaciones=" + visualizaciones + "]";
	}

}
