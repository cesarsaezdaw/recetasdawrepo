package es.daw.spring.recetas.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.daw.spring.recetas.model.dao.MiRecetaCreadaDAOInterface;
import es.daw.spring.recetas.model.dao.PaisDAOInterface;
import es.daw.spring.recetas.model.dao.PasosRecetaDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasFavoritasDAOInterface;
import es.daw.spring.recetas.model.dao.TipoAlimentoDAOInterface;
import es.daw.spring.recetas.model.data.MiRecetaCreada;
import es.daw.spring.recetas.model.data.Receta;
import es.daw.spring.recetas.model.data.RecetaFavorita;
import es.daw.spring.recetas.model.data.Usuario;
import es.daw.spring.recetas.utils.FormRecetas;


@Controller
public class MisRecetasCreadasController {

	@Autowired RecetasDAOInterface recetasDAO;
	@Autowired MiRecetaCreadaDAOInterface miRecetaCreadaDAO;
	@Autowired RecetasFavoritasDAOInterface recetasFavoritasDAO;
	@Autowired PasosRecetaDAOInterface pasosRecetaDAO;
	@Autowired PaisDAOInterface paisDAO;
	@Autowired TipoAlimentoDAOInterface tipoAlimentoDAO;	
		
	@RequestMapping(value="/misRecetasCreadas.ctr", method=RequestMethod.GET)
	public String misRecetasCreadas(Model model, HttpSession session) {	
				
	    Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();
		
		List<MiRecetaCreada> misRecetas = this.miRecetaCreadaDAO.findAll(emailUsuario);
		List<Receta> receta = new ArrayList<Receta>();
		
		for(MiRecetaCreada miReceta : misRecetas) {
	    	receta.add(recetasDAO.findById(miReceta.getNombreReceta()));
	    }
		
	    model.addAttribute("recetas", receta);
	   	    
		return "misRecetasCreadas";
	}
	
	@RequestMapping(value="/addMisRecetasCreadas.ctr", method=RequestMethod.POST)
	public String addMisRecetasCreadas(FormRecetas formRecetas, Model model, HttpSession session) {
		
		String nombreReceta = formRecetas.getNombreReceta();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();
		boolean aniadida = false;
		
		MiRecetaCreada miRecetaCreada = new MiRecetaCreada();
			
		//Comprobamos que no exista aún 
		try {
			miRecetaCreada = miRecetaCreadaDAO.findById(nombreReceta,emailUsuario);
		}catch(Exception e) {
			miRecetaCreada=null;
		}
			
		if(miRecetaCreada == null) {
			try {
				miRecetaCreadaDAO.insertMiReceta(nombreReceta, emailUsuario);
				aniadida = true;
			}catch(Exception e) {
				System.out.println("Error en la insercion en BBDD");
			}
		}else {
			System.out.println("Ya estaba en Mis Recetas Creadas");
		}		
		
		model.addAttribute("aniadida", aniadida);			
		Receta receta = recetasDAO.findById(nombreReceta);
		model.addAttribute("receta", receta);

		return "datosReceta";
	}
	
	@RequestMapping(value="/eliminarMiRecetaCreada.ctr", method=RequestMethod.POST)
	public String eliminarMiRecetaCreada(FormRecetas formRecetas, Model model, HttpSession session) {
		
		String nombreReceta = formRecetas.getNombreReceta();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();		
		boolean eliminada=false;

		try {			
			//Comprobamos que exista 
			MiRecetaCreada recetaCreada = this.miRecetaCreadaDAO.findById(nombreReceta,emailUsuario);
			
			if(recetaCreada != null) {
				//eliminamos la receta en la tabla mis_recetas_creadas
				this.miRecetaCreadaDAO.deleteMiReceta(nombreReceta, emailUsuario);
				
				//eliminamos la receta de la tabla favoritos(si existe)
				try {
					List<RecetaFavorita> favoritas = this.recetasFavoritasDAO.findAll(nombreReceta);
					if(!favoritas.isEmpty()) {
						this.recetasFavoritasDAO.deleteFavorita(nombreReceta, emailUsuario);
					}
				}catch(Exception e){
					System.out.println("No hay en favoritos");
				}
									
				//eliminamos la receta de la tabla recetas
				Receta receta = this.recetasDAO.findById(nombreReceta);
				this.recetasDAO.delete(receta);				
				eliminada=true;				
			}else {
				System.out.println("No estaba creada");
				eliminada= false;
			}
							
		}catch(Exception e){
			System.out.println("Error en la eliminación en BBDD");
		}
				
		model.addAttribute("eliminada", eliminada);
		
		return misRecetasCreadas(model,session);
	}		
	
}
