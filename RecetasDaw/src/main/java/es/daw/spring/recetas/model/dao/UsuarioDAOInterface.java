package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.Usuario;

public interface UsuarioDAOInterface {
	
	public List<Usuario> findAll();
	
	public Usuario findById(String email);
	
	public void insert(Usuario usuario);
	
	public void update(Usuario usuario);
	
	public void delete(Usuario usuario);
	
}
