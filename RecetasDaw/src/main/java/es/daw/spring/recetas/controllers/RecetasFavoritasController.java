package es.daw.spring.recetas.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.daw.spring.recetas.model.dao.PasosRecetaDAO;
import es.daw.spring.recetas.model.dao.RecetasDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasFavoritasDAOInterface;
import es.daw.spring.recetas.model.data.Receta;
import es.daw.spring.recetas.model.data.RecetaFavorita;
import es.daw.spring.recetas.model.data.Usuario;
import es.daw.spring.recetas.utils.FormRecetas;


@Controller
public class RecetasFavoritasController {

	@Autowired RecetasDAOInterface recetasDAO;
	@Autowired RecetasFavoritasDAOInterface recetasFavoritasDAO;
	@Autowired PasosRecetaDAO pasosRecetaDAO;
	@Autowired RecetasController recetasController;
		
	@RequestMapping(value="/recetasFavoritas.ctr", method=RequestMethod.GET)
	public String recetasFavoritas(Model model, HttpSession session) {	
		
		Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();	    
	    List<RecetaFavorita> favoritas = this.recetasFavoritasDAO.findAllByUsuario(emailUsuario);	    
	    List<Receta> receta = new ArrayList<Receta>();
	    
	    for(RecetaFavorita favorita : favoritas) {
	    	receta.add(recetasDAO.findById(favorita.getNombreReceta()));
	    }
		
	    model.addAttribute("recetas", receta);
	    
	    return "recetasFavoritas";
	}	
	
	@RequestMapping(value="/addFavoritas.ctr", method=RequestMethod.POST)
	public String addFavoritas(FormRecetas formRecetas, Model model, HttpSession session) {
		
		String nombreReceta = formRecetas.getNombreReceta();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();		
		boolean aniadida = false;		
		RecetaFavorita favorita = new RecetaFavorita();
			
		//Comprobamos que no exista aún 
		try {
			favorita = recetasFavoritasDAO.findById(nombreReceta,emailUsuario);
		}catch(Exception e) {
			favorita=null;
		}
			
		if(favorita == null) {
			try {
				recetasFavoritasDAO.insertFavorita(nombreReceta, emailUsuario);
				aniadida = true;
			}catch(Exception e) {
				System.out.println("Error en la insercion en BBDD");
			}
		}else {
			System.out.println("Ya estaba en favoritas");
		}		
		
		model.addAttribute("aniadida", aniadida);	

		return this.recetasController.datosReceta(formRecetas, model, session);
	}
	
	@RequestMapping(value="/quitarFavoritas.ctr", method=RequestMethod.POST)
	public String quitarFavoritas(FormRecetas formRecetas, Model model, HttpSession session) {
		
		String nombreReceta = formRecetas.getNombreReceta();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
	    String emailUsuario= usuario.getEmail();
		boolean eliminada = false;
		
		try {			
			//Comprobamos que no exista aún 
			RecetaFavorita favorita = recetasFavoritasDAO.findById(nombreReceta,emailUsuario);			
			if(favorita != null) {
				recetasFavoritasDAO.deleteFavorita(nombreReceta, emailUsuario);
				eliminada=true;
			}else {
				System.out.println("No estaba en favoritas");//????
			}							
		}catch(Exception e){
			System.out.println("Error en la eliminación en BBDD");
		}
				
		Receta receta = recetasDAO.findById(nombreReceta);
		model.addAttribute("receta", receta);		
		model.addAttribute("eliminada", eliminada);
		
		return recetasFavoritas(model, session);
	}
	
}
