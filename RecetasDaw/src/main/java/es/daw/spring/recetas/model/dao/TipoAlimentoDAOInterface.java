package es.daw.spring.recetas.model.dao;

import java.util.List;

import es.daw.spring.recetas.model.data.TipoAlimento;

public interface TipoAlimentoDAOInterface {
	
	public List<TipoAlimento> findAll();
	
	public TipoAlimento findById(String nombre);
	
	public void insert(TipoAlimento tipoAlimento);
}
