package es.daw.spring.recetas.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import es.daw.spring.recetas.model.dao.MiRecetaCreadaDAO;
import es.daw.spring.recetas.model.dao.PaisDAOInterface;
import es.daw.spring.recetas.model.dao.PasosRecetaDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasDAOInterface;
import es.daw.spring.recetas.model.dao.RecetasFavoritasDAOInterface;
import es.daw.spring.recetas.model.dao.TipoAlimentoDAOInterface;
import es.daw.spring.recetas.model.data.MiRecetaCreada;
import es.daw.spring.recetas.model.data.PasoReceta;
import es.daw.spring.recetas.model.data.Receta;
import es.daw.spring.recetas.model.data.RecetaFavorita;
import es.daw.spring.recetas.model.data.Usuario;
import es.daw.spring.recetas.utils.FormRecetas;

@Controller
public class RecetasController {

	@Autowired RecetasDAOInterface recetasDAO;
	@Autowired RecetasFavoritasDAOInterface recetasFavoritasDAO;
	@Autowired MisRecetasCreadasController miRecetaCreadaController;
	@Autowired PasosRecetaDAOInterface pasosRecetaDAO;
	@Autowired PaisDAOInterface paisDAO;
	@Autowired TipoAlimentoDAOInterface tipoAlimentoDAO;
	@Autowired MiRecetaCreadaDAO miRecetaCreadaDAO;

	@RequestMapping(value="/crearReceta.ctr", method=RequestMethod.GET)
	public String crearReceta(Model model, HttpSession session) {
				
		List<Receta> listaRecetas = this.recetasDAO.findAll();
		List<String> nombreReceta = new ArrayList<String>();
		for(Receta re : listaRecetas) {
			nombreReceta.add(re.getNombre());
		}

		model.addAttribute("listaRecetas", nombreReceta);
		model.addAttribute("paises", this.paisDAO.findAll());
		model.addAttribute("tiposAlimentos", this.tipoAlimentoDAO.findAll());
		
		return "crearReceta";
	}
	
	@RequestMapping(value="/datosReceta.ctr", method=RequestMethod.POST)	
	public String datosReceta(FormRecetas formRecetas, Model model, HttpSession session) {	
		
		String nombreReceta = formRecetas.getNombreReceta();					
		List<PasoReceta> pasos =  this.pasosRecetaDAO.findAll(nombreReceta);				
		List<RecetaFavorita> favoritasList = new ArrayList<>();
		List<String> favoritasListStr = new ArrayList<String>();
		Usuario usuario = (Usuario) session.getAttribute("usuario");    
		if(usuario != null) {
			String emailUsuario= usuario.getEmail();
			favoritasList = this.recetasFavoritasDAO.findAllByUsuario(emailUsuario);			
			for(RecetaFavorita re :favoritasList) {
				favoritasListStr.add(re.getNombreReceta());
			}
		}
	
		//actualizo número de vistas
		Receta receta = this.recetasDAO.findById(nombreReceta);
		Integer visualizaciones = receta.getVisualizaciones();
		visualizaciones =  visualizaciones + 1;
		receta.setVisualizaciones(visualizaciones);		
		this.recetasDAO.update(receta);
		
		//TOP 5
		List<Receta> recetas = this.recetasDAO.findAll(); // lista ordenada por numero de visitas
		List<Receta> topRecetas = new ArrayList<>();
		int i=0;
		for(Receta r : recetas) {
			if(i < 5) {
				topRecetas.add(r);
			}
			i++;
		}
				
		model.addAttribute("receta", recetasDAO.findById(nombreReceta));
		model.addAttribute("pasos", pasos);
		model.addAttribute("favoritasList", favoritasList);
		model.addAttribute("favoritasListStr", favoritasListStr);
		model.addAttribute("topRecetas", topRecetas);
				
		return "datosReceta";//jsp
	}
	
	@RequestMapping(value="/crearReceta.ctr", method=RequestMethod.POST)	
	public String crearReceta(FormRecetas formRecetas, Model model, HttpSession session) {

		Receta receta = new Receta();
		receta.setNombre(formRecetas.getNombreReceta());
		receta.setFoto(formRecetas.getFotoReceta().getOriginalFilename());
		receta.setDescripcion(formRecetas.getDescripcionReceta());
		receta.setNotas(formRecetas.getNotasReceta());
		receta.setPais(formRecetas.getPaisReceta());
		receta.setTipoAlimento(formRecetas.getTipoAlimentoReceta());
		receta.setDuracion(formRecetas.getDuracionReceta());
		receta.setNumeroPersonas(formRecetas.getNumeroPersonasReceta());
		receta.setVisualizaciones(0);
		
		MultipartFile foto = formRecetas.getFotoReceta();				
		File convFile = new File(foto.getOriginalFilename());
		try {
			convFile.createNewFile();
			FileOutputStream fos;
			fos = new FileOutputStream(convFile);
			fos.write(foto.getBytes());	
			fos.close();			

			File origen = convFile;
            File destino = new File("" + System.getProperty("user.dir") + "\\src\\main\\webapp\\resources\\img\\recetas\\" + receta.getTipoAlimento() + "\\" + foto.getOriginalFilename().toString());

            try {
            	InputStream in = new FileInputStream(origen);
                OutputStream out = new FileOutputStream(destino);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                	out.write(buf, 0, len);
                }
                in.close();
                out.close();
            } catch (IOException ioe){
            	ioe.printStackTrace();
            }
						
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		boolean recetaCreada = false;
		//INSERT
		if(receta.getNombre()!=null) {	
			try {
				this.recetasDAO.insert(receta);
				recetaCreada=true;			
			}catch(Exception e) {
				recetaCreada=false;
			}			
			//pasos					
			List<String> preparacion = new ArrayList<>();
			preparacion.add(formRecetas.getPreparacionRecetaPaso1());
			preparacion.add(formRecetas.getPreparacionRecetaPaso2());
			preparacion.add(formRecetas.getPreparacionRecetaPaso3());
			preparacion.add(formRecetas.getPreparacionRecetaPaso4());
			preparacion.add(formRecetas.getPreparacionRecetaPaso5());
			preparacion.add(formRecetas.getPreparacionRecetaPaso6());
			preparacion.add(formRecetas.getPreparacionRecetaPaso7());
			preparacion.add(formRecetas.getPreparacionRecetaPaso8());
			preparacion.add(formRecetas.getPreparacionRecetaPaso9());
			preparacion.add(formRecetas.getPreparacionRecetaPaso10());
			int i=0;
			for(String p : preparacion) {
				if(!p.isEmpty()) {
					PasoReceta paso = new PasoReceta();
					paso.setNombreReceta(formRecetas.getNombreReceta());
					paso.setNumPaso(++i);
					paso.setDescripcion(p.trim());
					this.pasosRecetaDAO.insertPasoReceta(paso);
				}			
			}

			//insertar en mis recetas creadas
			try {
				this.miRecetaCreadaController.addMisRecetasCreadas(formRecetas,model, session);
			}catch(Exception e) {}
						
		}		
		model.addAttribute("receta", this.recetasDAO.findById(receta.getNombre()));
		model.addAttribute("recetaCreada", recetaCreada);		
		model.addAttribute("pasos", this.pasosRecetaDAO.findAll(receta.getNombre()));
				
		//return "datosReceta";
		return this.datosReceta(formRecetas, model, session);
	}
	
	@RequestMapping(value="/gestionRecetas.ctr", method=RequestMethod.GET)
	public String gestionRecetas(Model model, HttpSession session) {	
				
	    List<Receta> recetas = recetasDAO.findAll();
	    model.addAttribute("recetas", recetas);

		return "gestionRecetas";
	}	
	
	@RequestMapping(value="/editarReceta.ctr", method=RequestMethod.POST)
	public String editarReceta(FormRecetas formRecetas, Model model, HttpSession session) {	
				
	    Receta receta = this.recetasDAO.findById(formRecetas.getNombreReceta());
	        
	    model.addAttribute("receta", receta);
	    model.addAttribute("nombreReceta", receta.getNombre());	      		
	    model.addAttribute("descripcionReceta", receta.getDescripcion());
	    model.addAttribute("notasReceta", receta.getNotas());	    
	    model.addAttribute("paisReceta", receta.getPais()); 
	    model.addAttribute("tipoAlimentoReceta", receta.getTipoAlimento());
	    model.addAttribute("duracionReceta", receta.getDuracion()); 
	    model.addAttribute("numeroPersonasReceta", receta.getNumeroPersonas());
 
	    boolean errorPasos=false;
	    List<PasoReceta> pasosReceta = this.pasosRecetaDAO.findAll(formRecetas.getNombreReceta());
	    if(pasosReceta.size() <= 10) {
	    	for(int i=1;i<=pasosReceta.size();i++) {	    	
		    	 model.addAttribute("preparacionRecetaPaso"+i, pasosReceta.get(i-1).getDescripcion());
		    }
	    }else {
	    	errorPasos = true;
	    }
	    
	    model.addAttribute("errorPasos", errorPasos);// si existe algun error en BBDD
	    model.addAttribute("paises", this.paisDAO.findAll());
		model.addAttribute("tiposAlimentos", this.tipoAlimentoDAO.findAll());
		
		return "editarReceta";
	}	
	
	@RequestMapping(value="/eliminarReceta.ctr", method=RequestMethod.POST)
	public String eliminarReceta(FormRecetas formRecetas, Model model, HttpSession session) {
		
		boolean eliminada = false;	
		
		//Eliminación de la receta en la tabla "recetas_favoritas" (varios usuarios pueden tener como vavoritos la misma receta)
		List<RecetaFavorita> favoritas = this.recetasFavoritasDAO.findAll(formRecetas.getNombreReceta());
		if(!favoritas.isEmpty()) {
			for(RecetaFavorita f : favoritas) {
				this.recetasFavoritasDAO.deleteFavorita(f.getNombreReceta(), f.getEmailUsuario());
			}
		}
		
		//Eliminación de la receta en la tabla "recetas_creadas". Solo un usuario ha podido crear la receta
		MiRecetaCreada creada = this.miRecetaCreadaDAO.findByNombreReceta(formRecetas.getNombreReceta());
		if(creada != null) {
			try {
				this.miRecetaCreadaDAO.deleteRecetaCreada(formRecetas.getNombreReceta());
				creada=null;
			}catch(Exception e) {}
			
		}
		
		//Eliminación de los pasos de la receta en la tabla "pasos_recetas"
		List<PasoReceta> pasos = this.pasosRecetaDAO.findAll(formRecetas.getNombreReceta());
		if(!pasos.isEmpty()) {
			this.pasosRecetaDAO.deletePasos(formRecetas.getNombreReceta());					
		}		
		
		favoritas = this.recetasFavoritasDAO.findAll(formRecetas.getNombreReceta());
		if(favoritas.isEmpty() && creada == null) {
			this.recetasDAO.deleteById(formRecetas.getNombreReceta());
			eliminada = true;
		}else {
			eliminada = false;
		}
		
	    model.addAttribute("eliminada", eliminada);
	    
	    List<Receta> recetas = recetasDAO.findAll();
	    model.addAttribute("recetas", recetas);
	    
	    return "gestionRecetas";
	}	
	
	@RequestMapping(value="/modificarReceta.ctr", method=RequestMethod.POST)
	public String modificarReceta(FormRecetas formRecetas, Model model, HttpSession session) {	
	
		Receta receta = this.recetasDAO.findById(formRecetas.getNombreReceta());	
		receta.setDescripcion(formRecetas.getDescripcionReceta());
		receta.setDuracion(formRecetas.getDuracionReceta());			
		receta.setNotas(formRecetas.getNotasReceta());
		receta.setNumeroPersonas(formRecetas.getNumeroPersonasReceta());
		receta.setPais(formRecetas.getPaisReceta());	
		receta.setTipoAlimento(formRecetas.getTipoAlimentoReceta());
		
		boolean modificada = true;
		//UPDATE
		try {
			this.recetasDAO.update(receta);
		}catch(Exception e) {
			modificada = false;
		}
		
		//pasos		
		this.pasosRecetaDAO.deletePasos(formRecetas.getNombreReceta());
		
		List<String> preparacion = new ArrayList<>();
		preparacion.add(formRecetas.getPreparacionRecetaPaso1());
		preparacion.add(formRecetas.getPreparacionRecetaPaso2());
		preparacion.add(formRecetas.getPreparacionRecetaPaso3());
		preparacion.add(formRecetas.getPreparacionRecetaPaso4());
		preparacion.add(formRecetas.getPreparacionRecetaPaso5());
		preparacion.add(formRecetas.getPreparacionRecetaPaso6());
		preparacion.add(formRecetas.getPreparacionRecetaPaso7());
		preparacion.add(formRecetas.getPreparacionRecetaPaso8());
		preparacion.add(formRecetas.getPreparacionRecetaPaso9());
		preparacion.add(formRecetas.getPreparacionRecetaPaso10());
		int i=0;
		for(String p : preparacion) {
			if(!p.isEmpty()) {
				PasoReceta paso = new PasoReceta();
				paso.setNombreReceta(formRecetas.getNombreReceta());
				paso.setNumPaso(++i);
				paso.setDescripcion(p.trim());
				try {
					this.pasosRecetaDAO.insertPasoReceta(paso);					
				}catch(Exception e) {
					modificada = false;
				}
				
			}			
		}
			
		List<Receta> recetas = recetasDAO.findAll();
		model.addAttribute("recetas", recetas);
		model.addAttribute("receta", this.recetasDAO.findById(receta.getNombre()));				
		model.addAttribute("pasos", this.pasosRecetaDAO.findAll(receta.getNombre()));
		model.addAttribute("modificada", modificada);
		
		return this.datosReceta(formRecetas, model, session);
	}	
	
}
